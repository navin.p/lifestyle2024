//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAuth

class WebService: NSObject,NSURLConnectionDelegate {
    
    
    class func callAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if let data = response.result.value
                {
                    OnResultBlock(data as! NSDictionary,"success")
                }
                break
                
            case .failure(_):
                //print(response.result.error)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
                
            }
            
        }
        
    }
    
    //MARK:
    //MARK: API UPLOAD IMAGE
    
    class func callAPIWithImageOnlyOne(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if isInternetAvailable() {
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
               
                
                multiPartFormData.append(image.pngData()!, withName: "photo_path", fileName: "\(imageName)", mimeType: "image/jpeg")
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        //                        print(response.request)  // original URL request
                        //                        print(response.response) // URL response
                        //                        print(response.data)     // server data
                        //                        print(response.result)   // result of response serialization
                        let statusCode = response.response?.statusCode
                        if statusCode == 200
                        {
                            let dic = NSMutableDictionary.init()
                            dic.setValue("\(response.result)", forKey: "message")
                            OnResultBlock(dic,"success")
                        }else{
                            let dic = NSMutableDictionary.init()
                            dic.setValue("FAILURE", forKey: "message")
                            OnResultBlock(dic,"failure")
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic.setValue("Connection Time Out", forKey: "message")
                    OnResultBlock(dic,"failure")
                    
                }
                
            }
            
        }else{
            
            let dic = NSMutableDictionary.init()
            dic.setValue(alertInternet, forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
    }
    
    
    
    //MARK:
    //MARK: API Without Secureity Parameter
    
    class func callAPIBYpostSaveOfflineData(parameter:String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let headers: HTTPHeaders = [
            "OfflineTreeTaggingDc": parameter,
            "Accept": "application/json"
        ]
        
        request(strUrlwithString!, method: .post, parameters:nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }    
    
    class func callAPIBYGET(parameter:NSDictionary, responseType:String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        if(responseType == "string")
        {
            
            request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { (response) in
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                        let dictdata = NSMutableDictionary()
                        let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                            "Result" == (k as AnyObject)as! String
                        })
                        if status{
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,"success")
                        }
                        else {
                            let dic = NSMutableDictionary.init()
                            dic .setValue("\(alertSomeError) ", forKey: "message")
                            OnResultBlock(dic,"failure")
                        }
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
            }
        }
        else
        {
            request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                        if(data is NSDictionary)
                        {
                            OnResultBlock((data as! NSDictionary) ,"success")
                        }
                        else if(data is NSArray)
                        {
                             let dictdata = NSMutableDictionary()
                            dictdata.setValue(data, forKey: "data")
                         
                            OnResultBlock((dictdata as NSDictionary) ,"success")

                        }
                        
//                        let dictdata = NSMutableDictionary()
//                        let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
//                            "Result" == (k as AnyObject)as! String
//                        })
//                        if status{
//                            dictdata.setValue(data, forKey: "data")
//                            OnResultBlock((dictdata) ,"success")
//                        }
//                        else {
//                            let dic = NSMutableDictionary.init()
//                            dic .setValue("\(alertSomeError) ", forKey: "message")
//                            OnResultBlock(dic,"failure")
//                        }
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
            }
            
        }
    }
    
    class func callAPIBYGETWhoseResponseIsString(parameter:NSDictionary, url:String,OnResultBlock: @escaping (_ strResponse: String,_ status:String) -> Void) {
        
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
            request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { (response) in
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                            OnResultBlock(data ,"success")
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock("false","failure")
                    break
                }
            }
    }
    
    class func callAPIBYPOST_string(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { (response:DataResponse<String>) in
             switch(response.result)   {
                case .success(_):
                if let data = response.result.value
                    {
                        // print(data)
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                }
                break
                case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        
        request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"success")
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    // service for multiple
    class func callAPIWithMultiImage(parameter:NSDictionary,url:String,imageArray:NSMutableArray,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            if(imageArray.count>0)
            {
                for (_, item ) in imageArray.enumerated()
                {
                    let img = ((item as AnyObject) as! NSDictionary).value(forKey: "image") as! UIImage
                    // let img = (item as AnyObject) as! UIImage
                    
                    let imgName = ((item as AnyObject) as! NSDictionary).value(forKey: "imageName") as! String
                    
                    let fileName = ((item as AnyObject) as! NSDictionary).value(forKey: "fileName") as! String
                   

                    if   let imageData = img.jpegData(compressionQuality:0.5) {
                        multiPartFormData.append(imageData, withName: "\(fileName)", fileName: imgName, mimeType: "image/png")
                    }
                }
                
            }
            
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                    print(response.request ?? 0)  // original URL request
                    print(response.response ?? 0) // URL response
                    print(response.data ?? 0)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value
                    {
                        OnResultBlock(JSON as! NSDictionary,"success")
                    }else{
                        let dic = NSMutableDictionary.init()
                        dic.setValue("Connection Time Out ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    
    class func callAPIWithMultiImageAndAudio(parameter:NSDictionary,url:String,imageArray:NSMutableArray,audioData:NSData,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            if(imageArray.count>0)
            {
                for (_, item ) in imageArray.enumerated()
                {
                   /* let img = ((item as AnyObject) as! NSDictionary).value(forKey: "image") as! UIImage
                    // let img = (item as AnyObject) as! UIImage
                    
                    let imgName = ((item as AnyObject) as! NSDictionary).value(forKey: "imageName") as! String
                    
                    let fileName = ((item as AnyObject) as! NSDictionary).value(forKey: "fileName") as! String
                    
                    
                    if   let imageData = img.jpegData(compressionQuality:0.5) {
                        multiPartFormData.append(imageData, withName: "\(fileName)", fileName: imgName, mimeType: "image/png")
                    }
                    */
                    
                   
                     let img = (item as AnyObject) as! UIImage
                    
                    //let imgName = ((item as AnyObject) as! NSDictionary).value(forKey: "imageName") as! String
                    
                   // let fileName = ((item as AnyObject) as! NSDictionary).value(forKey: "fileName") as! String
                    
                    let imageFileName = getCuurentTimeWithMilliseconds() + "Image.png"
                    if   let imageData = img.jpegData(compressionQuality:0.5) {
                      
                        multiPartFormData.append(imageData, withName: "image", fileName: imageFileName, mimeType: "image/png")
                    }                    
                }
            }
            if(audioData.length > 0)
            {
                let audioFileName = getCuurentTimeWithMilliseconds() + "Audio.m4a"
                multiPartFormData.append(audioData as Data, withName: "Audio", fileName: audioFileName, mimeType: "audio/m4a")
              //  multiPartFormData.append(audioData as Data, withName: "AudioFile.m4a", mimeType: "audio/m4a")
            }
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                    print(response.request ?? 0)  // original URL request
                    print(response.response ?? 0) // URL response
                    print(response.data ?? 0)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value
                    {
                        OnResultBlock(JSON as! NSDictionary,"success")
                    }else{
                        let dic = NSMutableDictionary.init()
                        dic.setValue("Connection Time Out ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    class func callAPIWithImageandName(parameter:NSDictionary,url:String,image:UIImage,imageName:String,fileParameter : String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
//            if  let imageData = image.pngData() {
//                multiPartFormData.append(imageData, withName: imageName, fileName: fileParameter, mimeType: "image/png")
//            }
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                    print(response.request ?? 0)  // original URL request
                    print(response.response ?? 0) // URL response
                    print(response.data ?? 0)     // server data
                    print(response.result)   // result of response serialization
                    if let JSON = response.result.value
                    {
                        OnResultBlock(JSON as! NSDictionary,"success")
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            let imageData = image.jpegData(compressionQuality:0.5)
            multiPartFormData.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                   print(response.request ?? 0)  // original URL request
                    print(response.response ?? 0) // URL response
                    print(response.data ?? 0)     // server data
                    print(response.result)   // result of response serialization
                    print(response.result.value)   // result of response serialization

                    if let JSON = response.result.value
                    {
                        OnResultBlock(JSON as! NSDictionary,"success")
                    }else{
                        let dic = NSMutableDictionary.init()
                   
                        OnResultBlock(dic,"failure")
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    func requestWith(endUrl: String, imageData: Data?, parameters: NSMutableDictionary, onCompletion: ((_ dict: NSDictionary) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            //"Content-type": "multipart/form-data"
            :]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            // multipartFormData.append("\(90)".data(using: String.Encoding.utf8)!, withName: "user_id")
            
            if let data = imageData{
                multipartFormData.append(data, withName: "File", fileName: "image.jpg", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?(NSDictionary())
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }

    
    //MARK:
    //MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    //MARK:
    //MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "mmddHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
 
    
    // MARK: - Convert String To Dictionary Function
    
    class func convertToDictionary(text: String) -> NSDictionary {
        do {
            
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Dictionary<String, Any>
            // print(data)
            let firstElement: NSDictionary = data! as NSDictionary
            return firstElement
        } catch {
            print(error.localizedDescription)
        }
        return NSDictionary()
    }
    
    class func convertToArray(text: String) ->NSArray {
        do {
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Array<Dictionary<String, Any>>
            //print(data)
            let firstElement: NSArray = data! as NSArray
            return firstElement
        }
        catch{
            print ("Handle error")
        }
        return NSArray()
    }
}
