//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import FirebaseAuth

var dots = DotsLoader()
var btnTransprantDots = UIButton()
var viewForDot = UIView()

//MARK:
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var storyboard_name = String()
var btnOverLay = UIButton()
var dict_Login_Data = NSMutableDictionary()
var db = FirebaseDB()
//MARK: Aler Messages

var greenColor = hexStringToUIColor(hex: "50BE89")
var orangeColor = "F6820D"
var primaryColor = "009A00"
var darkGreyColor = "414550"
var primaryGrayColor = "f7f7f7"
var borderColor = hexStringToUIColor(hex: "414550").cgColor
var appThemeColor = hexStringToUIColor(hex: "414550")
var cellBGColor = hexStringToUIColor(hex: "FFFB00")

var application_Version : String = "1.0.0"


var primaryFontLatoBold = "Lato-Bold"
var primaryFontLatoRegular = "Lato-Regular"
var application_ID : String = "https://itunes.apple.com/in/app/greengene/id1429856765?mt=8"

var Platform = "iPhone"
var Website = "https://www.google.com"




var AppName = "Lifestyle"
var alertMessage = "Alert!"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, please try again later."
var alertDataNotFound = "Sorry , Data is not Available!"
var alertSomeError = "Somthing went wrong, please try again later."

var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout?"


var Alert_OTP = "OTP required!"
var Alert_Required = "Required!"
var Alert_RequiredText = "Required!"

var Alert_Password = "Six digit's  required!"
var Alert_Mobile_limit = "Mobile number invalid!"
var Alert_Whatsapp_limit = "Whatsapp number invalid!"
var Alert_landline_limit = "Landline number invalid!"
var Alert_PinCode_limit = "Pincode  invalid!"


var Alert_EmailAddress = "Email address invalid!"
var Alert_CPassword = "Password did't match!"
var Alert_SelectOptionProfile = "Please Select an option for profile image"
var Alert_SelectOptionAddress = "Please Select an option for address proof"
var Alert_SelectOptionID = "Please Select an option for ID proof"
var Alert_SelectOptionOther = "Please Select an option for other documents"
var Alert_Gallery = "Gallery"
var Alert_Camera = "Camera"
var Alert_Preview = "Preview"
var Alert_SelectAddressDocument = "Please select address proof!"
var Alert_ID_proof = "Please select ID proof!"

var Alert_MeterImage = "Please capture meter image!"
var Alert_CameraAlert = "You don't have camera!"
var Alert_ReadingImageLimit = "Max limit reached!"
var Alert_PasswordLimit = "Password should be minimum 6 digits!"

var Alert_ProfileImage = "Profile Image required!"
var Alert_AadharNumberValid = "Aadhar number invalid!"
var Alert_PanNumberValid = "PAN card number invalid!"
var Alert_MeterReading = "Meter reading required!"

var check = "check.png"
var uncheck = "uncheck.png"
var dashboard = "dashboard.png"
var task = "task.png"
var transaction = "transaction.png"
var document = "documents.png"
var workorder = "workorders.png"
var changepassword = "changepassword.png"
var logout = "logout.png"
////MARK:
//MARK:  WEB SERVICES URL


// production

let BaseURL = "http://api.lifestyle2024.com//api/Customer/"

// testing

//let BaseURL = "http://sandboxapi.lifestyle2024.com//api/Customer/"

// API

let URL_DashboardDetails = BaseURL + "GetDashboardDetails"
let URL_GetPendingAgreements = BaseURL + "GetPendingAgreements"
let URL_GetAccountDocuments = BaseURL + "GetAccountDocuments"
let URL_GetTransactions = BaseURL + "GetTransactions"
let URL_GetWorkorderDetailForAllCompany = BaseURL + "GetWorkorderDetailForAllCompany"
let URL_ServiceComplaint = BaseURL + "ServiceComplaintForMobile" //"ServiceComplaint"
let URL_ServiceRequestForMobile = BaseURL + "ServiceRequestForMobile"
let URL_GeneratePaymentToken = BaseURL + "GeneratePaymentToken"
let URL_GetEmployeeOnTheWayLocation = BaseURL + "http://api.lifestyle2024.com//api/Customerapi/EmployeeCurrentLocation/GetEmployeeOnTheWayLocation"


//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
     static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}



//MARK:
//MARK: OTHER FUNCTION
func jsontoString(fromobject:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: fromobject, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func rateForApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : appId) else {
        completion(false)
        return
    }
    guard #available(iOS 10, *) else {
        completion(UIApplication.shared.openURL(url))
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
}

//MARK:
//MARK: DATE
func getTodayString() -> String{
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    let today_string = String(year!) + "_" + String(month!) + "_" + String(day!) + "_" + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    return today_string
}

func getUniqueString()-> String{
    var strName = "\(Date()).jpg".replacingOccurrences(of: "-", with: "")
    strName = strName.replacingOccurrences(of: " ", with: "")
    strName = strName.replacingOccurrences(of: "+", with: "")
    return  strName.replacingOccurrences(of: ":", with: "")
}



//MARK:
//MARK: txtFiledValidation
func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)

    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strValid == false{
            return false
        }
    }
    
  
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}

func txtViewValidation(textField : UITextView , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}


func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}

func panCardValidation(textField1 : ACFloatingTextfield , textField2 : UITextField, string: String , type : String) -> Bool{
    var textField = type == "1" ? ACFloatingTextfield(): UITextField()
     textField.text = type == "1" ? textField1.text: textField2.text
    
        if(textField.text?.count == 0)
        {
            if(string==" ")
            {
                return false
            }
            if(string=="")
            {
                return true
            }
        }
        let cs = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
        
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        if (string == filtered)
        {
            if(string==" ")
            {
                return false
            }
            if(string=="")
            {
                return true
            }
            if(textField.text!.count == 10)
            {
                return false
            }
            
            if((textField.text!.count >= 0) && (textField.text!.count <= 4))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) != nil
            }
            if((textField.text!.count > 4) && (textField.text!.count <= 8))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil
            }
            if((textField.text!.count > 7) && (textField.text!.count <= 9))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) != nil
            }
            return true
        }
        return false
    }

func buttonEnable_OnView(tag : Int , btn : UIButton)  {
    btn.layer.cornerRadius = 2.0
    if(tag == 1){
        btn.layer.borderWidth = 0.0
        btn.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btn.setTitleColor(UIColor.white, for: .normal)
    }else{
        btn.backgroundColor = UIColor.white
        btn.layer.borderWidth = 1.0
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.setTitleColor(UIColor.lightGray, for: .normal)
    }
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
{
    let dic = dict;
    for (key, value) in dict {
        let val : NSObject = value as! NSObject;
        if(val.isEqual(NSNull()))
        {
            dic.setValue("", forKey: (key as? String)!)
        } else if(val.isEqual("<null>"))
        {
            dic.setValue("", forKey: (key as? String)!)
        }
        else
        {
            dic.setValue(value, forKey: key as! String)
        }
    }
    return dic;
}



//MARK:
//MARK: Loader
func allButtonUserintractionClose(controller : UIViewController){
 //   btnOverLay.backgroundColor = UIColor.black
  //  btnOverLay.alpha = 1.0
        btnOverLay.frame = controller.view.frame
        controller.view.addSubview(btnOverLay)
}
func allButtonUserintractionEnable(controller : UIViewController)  {
   btnOverLay.removeFromSuperview()
}

/*
//MARK:
//MARK: Dots Loader
var dots = DotsLoader()
var btnTransprant = UIButton()
func dotLoader(sender : UIButton , controller : UIViewController ,viewContain : UIView , onView : String)  {
    allButtonUserintractionClose(controller: controller)

    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.tintColor = hexStringToUIColor(hex: primaryColor)
    dots.backgroundColor = UIColor.white
 
    if onView == "Button" {
        
        dots.frame =  sender.frame
        viewContain.addSubview(dots)
        
    }
    else if (onView == "MainView"){
        var height = 84
        if(DeviceType.IS_IPHONE_X){
            height = 104
        }
        dots.frame = CGRect(x: 0, y: height, width: Int(controller.view.frame.width), height: Int(controller.view.frame.height) - height)
        dots.backgroundColor = UIColor.clear
        controller.view.addSubview(dots)
        
    }
    else if (onView == "FullView"){
        var height = 84
        if(DeviceType.IS_IPHONE_X){
            height = 104
        }
        btnTransprant.frame = CGRect(x: 0, y: height, width: Int(controller.view.frame.width), height: Int(controller.view.frame.height) - height)
        btnTransprant.backgroundColor = UIColor.black
        btnTransprant.alpha = 0.5
        viewContain.addSubview(btnTransprant)
        dots.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        dots.layer.cornerRadius = 10.0
        dots.frame = CGRect(x:Int(viewContain.frame.width / 2)  - 100 , y: Int(viewContain.frame.height / 2 - 25), width: 200, height: 50)
        controller.view.addSubview(dots)
    }
}

func remove_dotLoader(controller : UIViewController)  {
  dots.removeFromSuperview()
   btnTransprant.removeFromSuperview()
  allButtonUserintractionEnable(controller: controller)
}
*/
func imageResize(image: UIImage, newSize: CGSize) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage ?? UIImage()
}

//MARK:
//MARK: Local Directory

func getImagefromDirectory(strname : String) -> UIImage{
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        if strname == ""{
            return UIImage()
        }else{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strname)
            if(imageURL.path.count != 0){
                return  UIImage(contentsOfFile: imageURL.path)!
            }
             return UIImage()
        }
    }
    return UIImage()
}


func removeImageFromDirectory(itemName:String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName)"
    print("Delete file Name : \(itemName)")
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }
}

//MARK:
//MARK: Related to date formet

func dateTimeConvertor(str: String , formet : String , strFormatToBeConverted : String) -> String {
    if str != "" && str != "<null>" {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formet
        let date = dateFormatter.date(from: str)!
        dateFormatter.dateFormat = strFormatToBeConverted
        let dateString1 = dateFormatter.string(from: date)
        let finlString = "\(dateString1)"
        if(finlString == ""){
            return  " "
        }
        return finlString
    }else{
        return "Not Available"
    }
}

func getDateFromString(str: String)-> Date{
   
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.date(from: str)!
        return date
}


func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}
//MARK:
//MARK: Animation

func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}






//MARK:
//MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

func calculateSizeFromString(message : String ) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0)])
    return size.height * 2
}
func calculateWidthFromString(message : String ) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0)])
    return size.width + 5
}
func calculateWidthTopFromString(message : String ) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)])
    return size.width + 5
}
func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
func showToastForSomeTime(title : String , message : String , time : Int, viewcontrol : UIViewController){
    let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
    viewcontrol.present(alert, animated: true, completion: nil)
    // change to desired number of seconds (in this case 5 seconds)
    let when = DispatchTime.now() + 2
    DispatchQueue.main.asyncAfter(deadline: when){
        // your code with delay
        alert.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:
//MARK:  Calling & Message & OpenMap & Attribute string

func setAttributeText(lblText : UILabel) {
    let lblPocCell = NSAttributedString(string: lblText.text!,attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
    lblText.attributedText = lblPocCell
}

func checkNullValue(str : String) -> String! {
    var strValue = String()
    if str == "nil" || str == "<null>" {
        strValue = ""
    }else{
        strValue = "\(str)"
    }
    return strValue
    
}



func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}

/*
//MARK:
//MARK:  Get Data from Local
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}



func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func getDataFromLocalUsingPredicate(strEntity: String ,pedicate : NSPredicate)-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = pedicate
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocalUsingAntity(entityName : String) -> NSMutableArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
    do {
        //go get the results
        return  (try AppDelegate.getContext().fetch(fetchRequest) as NSArray).mutableCopy() as! NSMutableArray
        
    } catch {
        print("Error with request: \(error)")
    }
    return NSMutableArray()
}



func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}
*/

func openMap(strTitle : String , strlat : String , strLong : String) {
    if !(strTitle == "<null>" || strTitle == "" || strTitle == "Not Available"){
        let latitude: CLLocationDegrees = Double(strlat)!
        let longitude: CLLocationDegrees = Double(strLong)!
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = strTitle
    
        mapItem.openInMaps(launchOptions: options)
    }
 
    
}
extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

func getFirebaseErrorMessage(errorCode : AuthErrorCode)-> String
{
    switch errorCode {
    
    case .userDisabled:
       return "Account is disabled on the server, please contact admin."
   
    case .emailAlreadyInUse:
        return "Email already exists."

    case .invalidEmail:
        return "Invalid email."

    case .wrongPassword:
        return "Password is wrong."
  
    case .userNotFound:
        return "Account does not found."
  
    case .networkError:
        return "Network error occurred, please try again later."
   
    case .credentialAlreadyInUse:
        return "Attempted to link with a credential that has already been linked with a different Firebase account."
   
    case .weakPassword:
        return "Password is too weak."
  
    case .invalidSender:
        return "Invalid email."

    case .invalidRecipientEmail:
        return "Invalid email."
  
    case .internalError:
        return "Some internal error occurred."

    default:
        return alertSomeError
    }
}

func getUserAccounts()-> NSMutableArray
{
    let arrayAccounts = (nsud.value(forKey: "useraccounts") as! NSArray).mutableCopy() as! NSMutableArray
    return arrayAccounts
}

func containsKey(dict : NSDictionary)-> Bool{
    
    let allKeys = dict.allKeys as NSArray
    let retVal = allKeys.contains("Message") as Bool
    return retVal
}

func checkForNull(val : String)-> String{
    
    if(val == "<null>" || val == "<Null>" || val == "null" || val == "Null"){
        
        return ""
    }
    
    return val
}
func getCurrencyFormat(str: String)-> String
{
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    // localize to your grouping and decimal separator
    currencyFormatter.locale = Locale.current
    
    // We'll force unwrap with the !, if you've got defined data you may need more error checking
    var priceString = currencyFormatter.string(from: NSNumber(value: Float(str)!))!
    print(priceString) // Displays $9,999.99 in the US locale
    
    priceString.remove(at: priceString.startIndex)
    
    return priceString
    
}
func deleteAllSavedDataFromUserDefaults()
{
    nsud.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    nsud.synchronize()
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
       
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        
    } else {
        
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}

func compressImage(image:UIImage) -> UIImage {
    
    let oldImage = image
    var imageData =  Data(oldImage.pngData()!)
    print("***** Uncompressed Size \(imageData.description) **** ")
    
    imageData = oldImage.jpegData(compressionQuality: 0.5)!

    print("***** Compressed Size \(imageData.description) **** ")
    
    let image = UIImage(data: imageData)
    return image!
    
}

func getDocumentsDirectory() -> URL {
    
    let path =  NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
    let url = URL(fileURLWithPath: path).appendingPathComponent("audioRecording.m4a")
    
    return url
}


func getAudioFileURL() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentDirectory =  paths[0] as URL
    let soundURL = documentDirectory.appendingPathComponent("audioRecording.m4a")
    return soundURL
}

func deleteAudioFile()
{
    let fileManager = FileManager.default

    do {
        try fileManager.removeItem(at: getAudioFileURL())
    }
    catch let error as NSError {
        print("Ooops! Something went wrong: \(error)")
    }

}

extension String {
    
    public var isAlphanumeric: Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,30}$")
        return passwordTest.evaluate(with: self)
    }
}
func getCuurentTimeWithMilliseconds()-> String
{
    let d = Date()
    let df = DateFormatter()
    df.dateFormat = "MM/dd/yyyy H:m:ss.SSSS"
    
  return  df.string(from: d) // -> "2016-11-17 17:51:15.1720"
}
func addAnimationOnView(view : UIView)
{
    let anim = CABasicAnimation(keyPath: "transform")
    anim.fromValue = CATransform3DMakeRotation(0.4, 1, 0, 0)
    anim.toValue = CATransform3DMakeRotation(-0.4, 1, 0, 0)
    anim.duration = 2
    anim.autoreverses = true
    anim.repeatCount = 500
    view.layer.add(anim, forKey: "transform")
    
//    view.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
//    view.layer.shadowRadius = 3.0;
//    view.layer.shadowOpacity = 0.6;
//    view.layer.shadowColor = UIColor.gray.cgColor
//    view.layer.masksToBounds = true;
//    view.layer.cornerRadius = view.frame.size.width/2
//     view.clipsToBounds = false
}

// MARK: loader methods
func customDotLoaderShowOnFull(message : String , controller : UIViewController){
  
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    
    btnTransprantDots.frame = controller.view.frame
    btnTransprantDots.backgroundColor = UIColor.black
    btnTransprantDots.alpha = 0.5
    controller.view.addSubview(btnTransprantDots)
    viewForDot = UIView()
    viewForDot.frame = CGRect(x: btnTransprantDots.frame.width / 2 - 75, y: btnTransprantDots.frame.height / 2 - 25, width: 150, height: 50)
    viewForDot.backgroundColor = UIColor.white
    viewForDot.layer.cornerRadius = 10.0
    controller.view.addSubview(viewForDot)
    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = CGRect(x: 5, y: 5, width: 140, height: 40)
    dots.tintColor = appThemeColor
    viewForDot.addSubview(dots)
    controller.view.addSubview(viewForDot)
    
}
func customeDotLoaderRemove(){
    for item in viewForDot.subviews {
        item.removeFromSuperview()
    }
    dots.removeFromSuperview()
    btnTransprantDots.removeFromSuperview()
    viewForDot.removeFromSuperview()
    
}

func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}
