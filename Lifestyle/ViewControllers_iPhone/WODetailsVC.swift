//
//  WODetailsVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 14/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class WODetailsVC: UIViewController {

    // outlets
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblWONo: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTechnicianName: UILabel!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var lblInvoiceAmt: UILabel!
    @IBOutlet weak var lblTax: UILabel!
  
    // variables
    var dictWODetails = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAccNo.text = "\(dictWODetails.value(forKey: "AccountNo")!)"
        if("\(dictWODetails.value(forKey: "WorkOrderNo")!)" == "<null>" || ("\(dictWODetails.value(forKey: "WorkOrderNo")!)").count == 0)
        {
           lblWONo.text = ""
        }
        else
        {
            lblWONo.text = "\(dictWODetails.value(forKey: "WorkOrderNo")!)"
        }
        
        lblService.text = "\(dictWODetails.value(forKey: "ServiceName")!)"
        if("\(dictWODetails.value(forKey: "TechnicianName")!)" == "<null>")
        {
            lblTechnicianName.text = ""
        }
        else
        {
            lblTechnicianName.text = "\(dictWODetails.value(forKey: "TechnicianName")!)"
        }
        lblScheduleDate.text = dateTimeConvertor(str: "\(dictWODetails.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
        
        // show invoice amount in currency format

        if(checkNullValue(str: "\(dictWODetails.value(forKey: "InvoiceAmount")!)")?.count == 0)
        {
             lblInvoiceAmt.text = "0.00"
        }
        else
        {
           lblInvoiceAmt.text = getCurrencyFormat(str: "\(dictWODetails.value(forKey: "InvoiceAmount")!)")
        }
        
        // show tax amount in currency format
        if(checkNullValue(str: "\(dictWODetails.value(forKey: "TaxAmount")!)")?.count == 0)
        {
             lblTax.text = "0.00"
        }
        else
        {
             lblTax.text = getCurrencyFormat(str: "\(dictWODetails.value(forKey: "TaxAmount")!)")
        }
        

    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /* @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
     
    
     
     }
     
     @IBAction func actionOnTransactions(_ sender: UIButton) {
    
     }
     
     @IBAction func actionOnPastServices(_ sender: UIButton) {
    
     
     @IBAction func actionOnTask(_ sender: Any) {
    
     }*/
    
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        
    }
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DocumentsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TransactionsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnPastServices(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is WorkOrdersVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TaskVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
}
