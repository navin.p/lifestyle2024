//
//  FilteredWorkOrderVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 04/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices
class FilteredWorkOrderVC: UIViewController {

    // outlet
    @IBOutlet weak var tblviewFilteredWO: UITableView!
    var strCompanyName = ""
    var strTechnicianName = ""
    var strFromDate = ""
    var strToDate = ""
    var strStatus = ""
    var arrayWorkOrders = NSMutableArray()
    var arrayWorkOrdersCopy = NSMutableArray()// this will be used as filtered array
    var dictWO = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblviewFilteredWO.tableFooterView = UIView()
        getAllWorkOrders()
    }
    
    // MARK:- web service
    func getAllWorkOrders(){
        
        if(isInternetAvailable())
        {
            let arrayAccounts = getUserAccounts()
            print(arrayAccounts)
            if(arrayAccounts.count > 0)
            {
                for (index, element) in arrayAccounts.enumerated(){
                    print("\(index)")
                    print("\(element)")
                    let dict = (element as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dict.setValue("All", forKey: "WoStatus")
                    arrayAccounts.replaceObject(at: index, with: dict)
                }
                print(arrayAccounts)
                
                if let url = NSURL(string:URL_GetWorkorderDetailForAllCompany){
                    
                    var request = URLRequest(url: url as URL)
                    
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.httpMethod = "POST"
                    
                    //  request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
                    
                    
                    request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
                    
//                    FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
                    
                    customDotLoaderShowOnFull(message: "", controller: self)
                    
                    Alamofire.request(request)
                        .responseJSON { response in
                            
//                            FTIndicator.dismissProgress()
                            customeDotLoaderRemove()
                            
                            switch response.result {
                            case .success(let responseObject):
                                
                                print(responseObject)
                                
                                self.arrayWorkOrders = (responseObject as! NSArray).mutableCopy() as! NSMutableArray
                                
                                self.arrayWorkOrdersCopy.removeAllObjects()
                                
                                for item  in self.arrayWorkOrders{
                                    
                                    let dict = item as! NSDictionary
                                    
                                    if(self.strStatus == "All")
                                    {
                                        self.strStatus = ""
                                    }
                                    
                                    if(self.strStatus.count > 0)
                                    {
                                        if(("\(dict.value(forKey: "WorkorderStatus")!)").lowercased() == self.strStatus.lowercased())
                                        {
                                            self.filterWorkOrder(dict: dict)
                                        }
                                    }
                                    else
                                    {
                                        self.filterWorkOrder(dict: dict)
                                    }
                                }
                                
                                if(self.arrayWorkOrdersCopy.count > 0)
                                {
                                    DispatchQueue.main.async {
                                        self.tblviewFilteredWO.reloadData()
                                    }
                                }
                                else
                                {
                                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
                                }
                                
                            case .failure(let error):
                                
                                print(error)
                                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                            }
                    }
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    func callConfirmAPI(dictParam:NSDictionary){
        
        if(isInternetAvailable()){
            
            let strUrl = BaseURL+"UpdateWorkOrderFlagToConfirmed?appKey=\(dictParam.value(forKey: "AppKey")!)&companyKey=\(dictParam.value(forKey: "CompanyKey")!)&WorkOrderNo=\(dictParam.value(forKey: "WorkOrderNo")!)"
            
//            FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
            customDotLoaderShowOnFull(message: "", controller: self)
            
            WebService.callAPIBYGETWhoseResponseIsString(parameter: NSDictionary(), url: strUrl) { (response, status) in
                
//                FTIndicator.dismissProgress()
                customeDotLoaderRemove()
                if(status == "success")
                {
                    if(response == "True" || response == "true" || response == "TRUE")
                    {
                        print(response)
                        self.getAllWorkOrders()
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                    print("some error occurred in confirm api")
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    func generatePaymentToken()
    {
        let dict = ["AppKey":"\(dictWO.value(forKey: "AppKey")!)",
            "CompanyKey":"\(dictWO.value(forKey: "CompanyKey")!)",
            "AccountNo":"\(dictWO.value(forKey: "AccountNo")!)",
            "EntityType":"Workorder",
            "EntityNumber":"\(dictWO.value(forKey: "WorkOrderNo")!)",
            "TransactionAmount":"2",//"\(dictWODetails.value(forKey: "TransactionAmount")!)"
            "ReturnURL":""]
        
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: dict,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
        }
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        WebService.callAPIBYPOST(parameter: dict as NSDictionary, url: URL_GeneratePaymentToken) { (response, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            
            print(status)
            print(response)
           
            if(status == "success")
            {
                if(("\(response.value(forKey: "data")!)").count > 0)
                {
                    let strURL = "http://pestream.com/payment/GeneralPayment/Index?tokenString=" + "\(response.value(forKey: "data")!)"
                    
                    let url = NSURL(string: (strURL ).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
                    
                    if(url != nil)
                    {
                        let safariVC = SFSafariViewController(url: url!)
                        safariVC.delegate = self
                        self.present(safariVC, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                print("some error occurred in confirm api")
            }
        }
    }
    
    // MARK: filter work orders
    func filterWorkOrder(dict: NSDictionary){
        
       if(self.strCompanyName.count == 0 && self.strTechnicianName.count == 0 && self.strFromDate.count == 0)
       {
          self.arrayWorkOrdersCopy.add(dict)
        }
       else{
        if(self.strCompanyName.count > 0)
        {
            if(self.strTechnicianName.count > 0)
            {
                if(self.strFromDate.count > 0)
                {
                    let strServiceDate =  dateTimeConvertor(str: "\(dict.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy")
                    
                    let serviceDate = getDateFromString(str: strServiceDate)
                    let fromDate = getDateFromString(str: self.strFromDate)
                    let toDate = getDateFromString(str: self.strToDate)
                    
                    if(("\(dict.value(forKey: "ProviderCompanyName")!)").lowercased() == self.strCompanyName.lowercased() && ("\(dict.value(forKey: "TechnicianName")!)").lowercased() == self.strTechnicianName.lowercased() && (serviceDate>=fromDate && serviceDate<=toDate))
                    {
                        self.arrayWorkOrdersCopy.add(dict)
                    }
                }
                else{
                    
                    if(("\(dict.value(forKey: "ProviderCompanyName")!)").lowercased() == self.strCompanyName.lowercased() && ("\(dict.value(forKey: "TechnicianName")!)").lowercased() == self.strTechnicianName.lowercased())
                    {
                        self.arrayWorkOrdersCopy.add(dict)
                    }
                }
            }
            else
            {
                if(self.strFromDate.count > 0)
                {
                    let strServiceDate =  dateTimeConvertor(str: "\(dict.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy")
                    
                    let serviceDate = getDateFromString(str: strServiceDate)
                    let fromDate = getDateFromString(str: self.strFromDate)
                    let toDate = getDateFromString(str: self.strToDate)
                    
                    if(("\(dict.value(forKey: "ProviderCompanyName")!)").lowercased() == self.strCompanyName.lowercased() && (serviceDate>=fromDate && serviceDate<=toDate))
                    {
                        self.arrayWorkOrdersCopy.add(dict)
                    }
                }
                else{
                    if(("\(dict.value(forKey: "ProviderCompanyName")!)").lowercased() == self.strCompanyName.lowercased())
                    {
                        self.arrayWorkOrdersCopy.add(dict)
                    }
                }
            }
        }
        else if(self.strTechnicianName.count > 0)
        {
            if(self.strFromDate.count > 0)
            {
                let strServiceDate =  dateTimeConvertor(str: "\(dict.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy")
                
                let serviceDate = getDateFromString(str: strServiceDate)
                let fromDate = getDateFromString(str: self.strFromDate)
                let toDate = getDateFromString(str: self.strToDate)
                
                if(("\(dict.value(forKey: "TechnicianName")!)").lowercased() == self.strTechnicianName.lowercased() && (serviceDate>=fromDate && serviceDate<=toDate))
                {
                    self.arrayWorkOrdersCopy.add(dict)
                }
            }
            else{
                
                if(("\(dict.value(forKey: "TechnicianName")!)").lowercased() == self.strTechnicianName.lowercased())
                {
                    self.arrayWorkOrdersCopy.add(dict)
                }
            }
        }
        else if(self.strFromDate.count > 0)
        {
            let strServiceDate =  dateTimeConvertor(str: "\(dict.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy")
            
            let serviceDate = getDateFromString(str: strServiceDate)
            let fromDate = getDateFromString(str: self.strFromDate)
            let toDate = getDateFromString(str: self.strToDate)
            
            if( (serviceDate>=fromDate && serviceDate<=toDate))//"\(dict.value(forKey: "TechnicianName")!)" == self.strTechnicianName &&
            {
                self.arrayWorkOrdersCopy.add(dict)
            }
        }
        }
    }
    
    // MARK: tableview cell button action
    @objc func actionOnEmployeeName(sender:UIButton!)
    {
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let employeeVC = storyboard.instantiateViewController(withIdentifier: "EmployeeProfileVC") as! EmployeeProfileVC
        employeeVC.dictData = dictWO
        self.navigationController?.pushViewController(employeeVC, animated: true)
        
    }
    @objc func actionOnOrderNumber(sender:UIButton!){
        
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let woDetailsVC = storyboard.instantiateViewController(withIdentifier: "WODetailsVC") as! WODetailsVC
        woDetailsVC.dictWODetails = dictWO
        self.navigationController?.pushViewController(woDetailsVC, animated: true)
    }
    
    @objc func actionOnConfirm(sender:UIButton!){
        
        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to confirm?", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: { (alertAction) in
            
            let dict = self.arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
            print(dict)
            self.callConfirmAPI(dictParam: dict)
            
        }))
        alertController.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (alertAction) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func actionOnReschedule(sender:UIButton!){
        
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rescheduleVC = storyboard.instantiateViewController(withIdentifier: "RescheduleWorkOrderVC") as! RescheduleWorkOrderVC
        rescheduleVC.dictWO = dictWO
        self.navigationController?.pushViewController(rescheduleVC, animated: true)
    }
    
    @objc private func actionOnSignature(sender:UIButton)
    {
        let dictTemp = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let signLink = "\(dictTemp.value(forKey: "SignatureLinkforAgree")!)"
        
        let url = NSURL(string: signLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
        
        if(url != nil)
        {
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
    }
    
    //
    @objc private func actionOnServiceComplain(sender:UIButton)
    {
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let serviceComplainVC = storyboard.instantiateViewController(withIdentifier: "SurveyComplainVC") as! SurveyComplainVC
        serviceComplainVC.dictWO = dictWO
        serviceComplainVC.strTitle =  "Service Issue"
        self.navigationController?.pushViewController(serviceComplainVC, animated: true)
    }
    
    @objc private func actionOnSurvey(sender:UIButton)
    {
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        if(dictWO.value(forKey: "SurveyURL") != nil && "\(dictWO.value(forKey: "SurveyURL")!)" != "<null>")
        {
            let strURL = dictWO.value(forKey: "SurveyURL")
            
            let url = NSURL(string: (strURL as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
            
            if(url != nil)
            {
                let safariVC = SFSafariViewController(url: url!)
                safariVC.delegate = self
                self.present(safariVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func actionOnReview(sender:UIButton)
    {
        
    }
    @objc private func actionOnPayBill(sender:UIButton)
    {
        print("Payment button clicked")
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        print(dictWO)
        generatePaymentToken()
        
    }
    
    @objc private func actionOnDocuments(sender:UIButton)
    {
        dictWO = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "WODocumentsVC") as! WODocumentsVC
        docVC.dictWO = dictWO
        self.navigationController?.pushViewController(docVC, animated: true)
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension FilteredWorkOrderVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayWorkOrdersCopy.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cellUpcoming = tableView.dequeueReusableCell(withIdentifier: "CellUpcomingActivities") as? CellUpcomingActivities
        
        if(cellUpcoming == nil)
        {
            cellUpcoming = (UITableViewCell.init(style: .default, reuseIdentifier: "CellUpcomingActivities") as! CellUpcomingActivities)
        }
        
        var cellCompleted = tableView.dequeueReusableCell(withIdentifier: "CellCompletedActivities") as? CellCompletedActivities
        
        if(cellCompleted == nil)
        {
            cellCompleted = (UITableViewCell.init(style: .default, reuseIdentifier: "CellCompletedActivities") as! CellCompletedActivities)
        }
        
        
        let dictData = arrayWorkOrdersCopy.object(at: indexPath.row) as! NSDictionary
        
        if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Completed").lowercased() || ("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Cancelled").lowercased()){
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellCompleted!.imgViewCompanyLogo)
                    cellCompleted!.imgViewCompanyLogo.image = image
                    
                }
            })
            
            cellCompleted!.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
            
            cellCompleted!.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
              cellCompleted!.btnEmployeeName.setTitle("", for: .normal)
            }
            else
            {
               cellCompleted!.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
            
            
            cellCompleted!.btnEmployeeName.tag = indexPath.row
            
            cellCompleted!.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellCompleted!.lblDateTime.text = ""
            }
            else
            {
                cellCompleted!.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellCompleted!.btnSurvey.layer.cornerRadius = cellCompleted!.btnSurvey.frame.size.height/2
            cellCompleted!.btnSurvey.tag = indexPath.row
            cellCompleted!.btnSurvey.addTarget(self, action: #selector(actionOnSurvey), for: .touchUpInside)
            
            
            cellCompleted!.btnPay.layer.cornerRadius = cellCompleted!.btnPay.frame.size.height/2
            cellCompleted!.btnPay.tag = indexPath.row
            cellCompleted!.btnPay.addTarget(self, action: #selector(actionOnPayBill), for: .touchUpInside)
            
            
            cellCompleted!.btnServiceComplain.layer.cornerRadius = cellCompleted!.btnServiceComplain.frame.size.height/2
            cellCompleted!.btnServiceComplain.tag = indexPath.row
            cellCompleted!.btnServiceComplain.addTarget(self, action: #selector(actionOnServiceComplain), for: .touchUpInside)
            
            
            cellCompleted!.btnDocuments.layer.cornerRadius = cellCompleted!.btnDocuments.frame.size.height/2
            cellCompleted!.btnDocuments.tag = indexPath.row
            cellCompleted!.btnDocuments.addTarget(self, action: #selector(actionOnDocuments), for: .touchUpInside)
            
            
            if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Cancelled").lowercased())
            {
                cellCompleted!.btnSurvey.isHidden = true
                cellCompleted!.btnServiceComplain.isHidden = true
                cellCompleted!.btnDocuments.isHidden = true
                cellCompleted!.viewVertical1.isHidden = true
                cellCompleted!.viewVertical2.isHidden = true
                cellCompleted!.btnPay.isHidden = true
            }
            else
            {
                
                if("\(dictData.value(forKey: "InvoiceStatus")!)" == "Closed" || "\(dictData.value(forKey: "InvoiceStatus")!)" == "closed")
                {
                    cellCompleted!.btnPay.isHidden = false
                    cellCompleted!.btnPay.isUserInteractionEnabled = false
                    cellCompleted!.btnPay.backgroundColor = greenColor
                    cellCompleted!.btnPay.setTitle("Paid", for: .normal)
                }
                else
                {
                    if let transactionAmount = dictData.value(forKey: "TransactionAmount") as? NSNumber {
                        let amt = transactionAmount.floatValue
                        
                        if(amt > 0.0)
                        {
                            cellCompleted!.btnPay.isHidden = false
                            cellCompleted!.btnPay.isUserInteractionEnabled = true
                            cellCompleted!.btnPay.backgroundColor = UIColor.red
                            cellCompleted!.btnPay.setTitle("Pay Bill", for: .normal)
                        }
                        else
                        {
                            cellCompleted!.btnPay.isHidden = true
                        }
                    }
                    
                }
                cellCompleted!.btnSurvey.isHidden = false
                cellCompleted!.btnServiceComplain.isHidden = false
                cellCompleted!.btnDocuments.isHidden = false
                cellCompleted!.viewVertical1.isHidden = false
                cellCompleted!.viewVertical2.isHidden = false
            }
            
            return cellCompleted!
            
        }
        
        
        if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("InComplete").lowercased() || ("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Reset").lowercased())
        {
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellUpcoming!.imgViewCompanyLogo)
                    cellUpcoming!.imgViewCompanyLogo.image = image
                    
                }
            })
        
            cellUpcoming!.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
            
            cellUpcoming!.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
              cellUpcoming!.btnEmployeeName.setTitle("", for: .normal)
            }
            else
            {
               cellUpcoming!.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
            
            
            cellUpcoming!.btnEmployeeName.tag = indexPath.row
            
            cellUpcoming!.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellUpcoming!.lblDateTime.text = ""
            }
            else
            {
                cellUpcoming!.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellUpcoming!.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
            
            cellUpcoming!.btnOrderNo.tag = indexPath.row
            cellUpcoming!.btnOrderNo.addTarget(self, action: #selector(actionOnOrderNumber), for: .touchUpInside)
            
            cellUpcoming!.btnConfirm.tag = indexPath.row
            cellUpcoming!.btnConfirm.layer.cornerRadius =  cellUpcoming!.btnConfirm.frame.size.height/2
            
            if("\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "true" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "True" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "TRUE" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "1")
            {
                cellUpcoming!.btnConfirm.setTitle("Confirmed", for: .normal)
                cellUpcoming!.btnConfirm.isUserInteractionEnabled = false
                cellUpcoming!.btnConfirm.alpha = 0.5
            }
            else
            {
                cellUpcoming!.btnConfirm.setTitle("Confirm", for: .normal)
                
                cellUpcoming!.btnConfirm.isUserInteractionEnabled = true
                
                cellUpcoming!.btnConfirm.addTarget(self, action: #selector(actionOnConfirm), for: .touchUpInside)
                cellUpcoming!.btnConfirm.alpha = 1
            }
            
            cellUpcoming!.btnReschedule.layer.cornerRadius =  cellUpcoming!.btnReschedule.frame.size.height/2
            cellUpcoming!.btnReschedule.tag = indexPath.row
            cellUpcoming!.btnReschedule.addTarget(self, action: #selector(actionOnReschedule), for: .touchUpInside)
            
            if("\(dictData.value(forKey: "WOOnMyWay")!)" == "true" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "True" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "TRUE" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "1" && "\(dictData.value(forKey: "TimeIn")!)" == "<null>")
            {
                cellUpcoming!.btnConfirm.isHidden = true
                cellUpcoming!.btnReschedule.isHidden = true
                cellUpcoming!.contentView.backgroundColor = UIColor.yellow
            }
            else
            {
                cellUpcoming!.btnConfirm.isHidden = false
                cellUpcoming!.btnReschedule.isHidden = false
                cellUpcoming!.contentView.backgroundColor = UIColor.white
            }
            
           if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Reset").lowercased())
            {
                cellUpcoming!.constLeadingConfirmBtn.constant = 0.0
                cellUpcoming!.constWidthConfirmBtn.constant = 0.0
            }
            else
           {
              cellUpcoming!.constLeadingConfirmBtn.constant = 10.0
              cellUpcoming!.constWidthConfirmBtn.constant = 100.0
            }
            
            return cellUpcoming!
        }
        
      /*  if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Reset").lowercased()){
            
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellCompleted.imgViewCompanyLogo)
                    cellCompleted.imgViewCompanyLogo.image = image
                    
                }
            })
            
            cellCompleted.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
            
            cellCompleted.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"
            
            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
               cellCompleted.btnEmployeeName.setTitle("", for: .normal)
            }
            else
            {
                cellCompleted.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellCompleted.lblDateTime.text = ""
            }
            else
            {
                cellCompleted.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellCompleted.btnSurvey.isHidden = true
            cellCompleted.btnSurvey.layer.cornerRadius = cellCompleted.btnSurvey.frame.size.height/2
            cellCompleted.btnSurvey.tag = indexPath.row
            cellCompleted.btnSurvey.addTarget(self, action: #selector(actionOnSurvey), for: .touchUpInside)
            
            
            cellCompleted.btnPay.isHidden = true
            cellCompleted.btnPay.layer.cornerRadius = cellCompleted.btnPay.frame.size.height/2
            cellCompleted.btnPay.tag = indexPath.row
            cellCompleted.btnSurvey.addTarget(self, action: #selector(actionOnPayBill), for: .touchUpInside)
            
            
            cellCompleted.btnServiceComplain.isHidden = true
            cellCompleted.btnServiceComplain.layer.cornerRadius = cellCompleted.btnServiceComplain.frame.size.height/2
            cellCompleted.btnServiceComplain.tag = indexPath.row
            cellCompleted.btnServiceComplain.addTarget(self, action: #selector(actionOnServiceComplain), for: .touchUpInside)
            
            
            cellCompleted.btnDocuments.isHidden = false
            cellCompleted.btnDocuments.layer.cornerRadius = cellCompleted.btnDocuments.frame.size.height/2
            cellCompleted.btnDocuments.tag = indexPath.row
            cellCompleted.btnDocuments.addTarget(self, action: #selector(actionOnDocuments), for: .touchUpInside)
            
            cellCompleted.viewVertical1.isHidden = false
            cellCompleted.viewVertical2.isHidden = false
            
            return cellCompleted
            
        }
       */
     
       /* if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Cancelled").lowercased()){
            
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellCompleted.imgViewCompanyLogo)
                    cellCompleted.imgViewCompanyLogo.image = image
                    
                }
            })
            
            cellCompleted.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
            
            cellCompleted.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"
            
            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
               cellCompleted.btnEmployeeName.setTitle("", for: .normal)
            }
            else
            {
                cellCompleted.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
           
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellCompleted.lblDateTime.text = ""
            }
            else
            {
                cellCompleted.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellCompleted.btnSurvey.isHidden = true
            cellCompleted.btnSurvey.layer.cornerRadius = cellCompleted.btnSurvey.frame.size.height/2
            cellCompleted.btnSurvey.tag = indexPath.row
            cellCompleted.btnSurvey.addTarget(self, action: #selector(actionOnSurvey), for: .touchUpInside)
            
            
            cellCompleted.btnPay.isHidden = true
            cellCompleted.btnPay.layer.cornerRadius = cellCompleted.btnPay.frame.size.height/2
            cellCompleted.btnPay.tag = indexPath.row
            cellCompleted.btnSurvey.addTarget(self, action: #selector(actionOnPayBill), for: .touchUpInside)
            
            
            cellCompleted.btnServiceComplain.isHidden = true
            cellCompleted.btnServiceComplain.layer.cornerRadius = cellCompleted.btnServiceComplain.frame.size.height/2
            cellCompleted.btnServiceComplain.tag = indexPath.row
            cellCompleted.btnServiceComplain.addTarget(self, action: #selector(actionOnServiceComplain), for: .touchUpInside)
            
            
            cellCompleted.btnDocuments.isHidden = true
            cellCompleted.btnDocuments.layer.cornerRadius = cellCompleted.btnDocuments.frame.size.height/2
            cellCompleted.btnDocuments.tag = indexPath.row
            cellCompleted.btnDocuments.addTarget(self, action: #selector(actionOnDocuments), for: .touchUpInside)
            
            cellCompleted.viewVertical1.isHidden = true
            cellCompleted.viewVertical2.isHidden = true
            
            return cellCompleted
            
        }*/
       
        
     /*
        cellPending.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
        cellPending.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"
        cellPending.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ScheduleStartDate")!)", formet: "yyyy-MM-dd'T'HH:mm:ss'Z'", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
        cellPending.lblLeadNo.text = "Lead#: \(dictData.value(forKey: "LeadNumber")!)"
        cellPending.lblDocType.text = "\(dictData.value(forKey: "DocType")!)"
        
        cellPending.btnSignatureLink.tag = indexPath.row
        cellPending.btnSignatureLink.layer.cornerRadius = cellPending.btnSignatureLink.frame.size.height/2
        
        cellPending.btnSignatureLink.addTarget(self, action: #selector(actionOnSignature), for: .touchUpInside)
 
        
        return cellPending
         */
        
        return cellCompleted!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
         let dictData = arrayWorkOrdersCopy.object(at: indexPath.row) as! NSDictionary
        
        if(("\(dictData.value(forKey: "WorkorderStatus")!)").lowercased() == ("Cancelled").lowercased())
        {
            return 135.0
        }
        
        return 160
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
extension FilteredWorkOrderVC:SFSafariViewControllerDelegate{
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }
}
