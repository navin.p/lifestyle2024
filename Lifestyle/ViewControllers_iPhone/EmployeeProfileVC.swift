//
//  EmployeeProfileVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 14/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class EmployeeProfileVC: UIViewController {

    // outlets
    @IBOutlet weak var imgviewCompanyLogo: UIImageView!
    @IBOutlet weak var imgviewProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNickName: UILabel!
    @IBOutlet weak var lblCellNo: UILabel!    
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    // variables
    var dictData = NSDictionary()
    var strCompanyKey = String()
    var strEmployeeNo = String()
    var strAppKey =  String()
    var blurEffectView = UIVisualEffectView()
    var strComeFrom = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(strComeFrom == "NewFlow"){
           
            self.imgviewProfile.layer.cornerRadius = self.imgviewProfile.frame.size.height/2
                              self.imgviewProfile.layer.masksToBounds = true
               self.imgviewProfile.image = dictData.value(forKey: "TechImage")as! UIImage
            self.lblName.text = "\(dictData.value(forKey: "name")!)"
        }else{
               strCompanyKey = "\(dictData.value(forKey: "CompanyKey")!)"
                    strEmployeeNo = "\(dictData.value(forKey: "EmployeeNo")!)"
                    strAppKey = "\(dictData.value(forKey: "AppKey")!)"
                    
                    self.imgviewProfile.layer.cornerRadius = self.imgviewProfile.frame.size.height/2
                    self.imgviewProfile.layer.masksToBounds = true
                    
                    let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
                    SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                        
                    }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                        
                        if image != nil {
                            
                            addAnimationOnView(view: self.imgviewCompanyLogo)
                            self.imgviewCompanyLogo.image = image
                        }
                        
                    })
                    
            //        SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, options: .continueInBackground, progress: nil, completed: {(image:UIImage?, data:Data?, error:Error?, finished:Bool) in
            //            if image != nil {
            //
            //                DispatchQueue.main.async {
            //                    self.imgviewCompanyLogo.image = image
            //                }
            //            }
            //        })
                    
                    getEmployeeProfile()
        }
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.blurEffects()
        self.animateImage()
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.blurEffects()
//        self.animateImage()
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getEmployeeProfile()
    {
        //http://api.lifestyle2024.com/api/Customer/GetEmployeeDetail?appKey=pestream&companyKey=dream.service&employeeNo=2
    
       // let url = "http://api.lifestyle2024.com/api/Customer/GetEmployeeDetail?appKey=\(strAppKey)&companyKey=\(strCompanyKey)&employeeNo=\(strEmployeeNo)"
        
        let url = BaseURL+"GetEmployeeDetail?appKey=\(strAppKey)&companyKey=\(strCompanyKey)&employeeNo=\(strEmployeeNo)"
        
        print(url)
        
//        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        WebService.callAPIBYGET(parameter: NSDictionary(), responseType: "json", url: url) { (dictData, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            
            if(status == "success")
            {
                if(containsKey(dict: dictData) == true)// error occurred
                {
                  showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                }
                else{
                    self.showEmployeeDetails(dict: dictData)
                }
                print(dictData)
            }
            else{
                
            }
        }
    }
    
    func showEmployeeDetails(dict : NSDictionary)
    {
        if("\(dict.value(forKey: "EmployeePhoto")!)" == "<null>" || ("\(dict.value(forKey: "EmployeePhoto")!)").count == 0)
        {
            
        }
        else{
          
            let url = URL(string: "\(dict.value(forKey: "EmployeePhoto")!)")
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, options: .continueInBackground, progress: nil, completed: {(image:UIImage?, data:Data?, error:Error?, finished:Bool) in
                        if image != nil {
            
                            DispatchQueue.main.async {
                                self.imgviewProfile.image = image
                            }
                        }
                    })
        }
        
        lblName.text = "\(checkNullValue(str: "\(dict.value(forKey: "FirstName")!)")!)" + " " + "\(checkNullValue(str: "\(dict.value(forKey: "LastName")!)")!)"
        if(lblName.text?.count == 0)
        {
            lblName.text = " "
        }
        lblNickName.text = lblName.text
        
        if(lblNickName.text?.count == 0)
        {
            lblNickName.text = " "
        }
        
        lblCellNo.text = "\(checkNullValue(str: "\(dict.value(forKey: "PrimaryPhone")!)")!)"
        if(lblCellNo.text?.count == 0)
        {
            lblCellNo.text = " "
        }
    }
    
    func blurEffects() {
        
        let blurEffect = UIBlurEffect(style: .light)
        
        if (blurEffectView.frame.size.width==0) {
            
            blurEffectView = UIVisualEffectView(effect: blurEffect)
//            blurEffectView.frame = imgViewBackground.frame
            blurEffectView.frame = self.view.frame
            
            imgViewBackground.insertSubview(blurEffectView, at: 0)
            
        }
        
    }
    
    func animateImage() {
        
        imgviewProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgviewProfile.transform = .identity
            },
                       completion: nil)
        
    }
}
