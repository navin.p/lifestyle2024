//
//  SurveyComplainVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 01/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos
// UICollectionView Cell
class CellImageServiceIssue: UICollectionViewCell,AVAudioPlayerDelegate {
    
    
    @IBOutlet weak var imgview: UIImageView!
}


class SurveyComplainVC: UIViewController,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    
    // outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtviewMsg: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnAddImages: UIButton!
    @IBOutlet weak var collectionViewImagesServiceIssue: UICollectionView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var hghtContCollectionView: NSLayoutConstraint!
    @IBOutlet weak var btnSeverity: UIButton!
    
    
    // variables
    var dictWO = NSDictionary()
    let imagePicker = UIImagePickerController()
    var arrayImages = NSMutableArray()
    var effectView = UIVisualEffectView()
    var audioPlayer : AVAudioPlayer!
    var strTitle = NSString()
    private  var strSeverity = "None"
    private var arraySeverity = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtviewMsg.text = "Message..."
        txtviewMsg.textColor = UIColor.lightGray
        txtviewMsg.layer.borderWidth = 1.0
        txtviewMsg.layer.borderColor = borderColor
        txtviewMsg.layer.cornerRadius = 2.0
        
        btnSave.layer.cornerRadius =  btnSave.frame.size.height/2
        btnSave.layer.masksToBounds = true
        
        btnSeverity.layer.borderWidth = 1.0
        btnSeverity.layer.borderColor = borderColor
        btnSeverity.layer.cornerRadius = 2.0        
        
        hghtContCollectionView.constant = 0.0;
     
        if(strTitle == "Service Issue")
        {
            lblTitle.text = "Service Issue"
        }
        else
        {
            lblTitle.text = "Service Request"
        }
        
        arraySeverity = ["None", "Low", "Normal", "High", "Urgent"]
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if(FileManager.default.fileExists(atPath: getAudioFileURL().path))
        {
            btnPlayAudio.isHidden = false
        }
        else{
            btnPlayAudio.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(audioPlayer != nil)
        {
            if audioPlayer.isPlaying
            {
                audioPlayer.stop()
            }
        }
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if(txtviewMsg.textColor == UIColor.lightGray || txtviewMsg.text.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter message", viewcontrol: self)
        }
        else{// call reschedule api
            if(isInternetAvailable())
            {
                if(strTitle == "Service Issue")
                {
                     serviceComplain()
                }
                else
                {
                     serviceRequest()
                }
             
            }
            else{
                 showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        deleteAudioFile()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnAddImage(_ sender: UIButton) {
        
      openCameraOrGallery()
    }
    
   
    @IBAction func actionOnRecordAudio(_ sender: UIButton) {
       
        
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        
        switch cameraAuthorizationStatus {
        case .denied:
            openSettingsForMicrphone()
            break
        case .authorized:
            self.gotoRecordAudioViewController()
            break
        case .restricted:
            openSettingsForMicrphone()
            break
            
        case .notDetermined:
            // Prompting user for the permission to use the microphone.
            AVCaptureDevice.requestAccess(for: AVMediaType.audio) { granted in
                if granted {
                    self.gotoRecordAudioViewController()
                } else {
                    self.openSettingsForMicrphone()
                }
            }
        }
    }
    
    func gotoRecordAudioViewController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let recordAudioVC = storyboard.instantiateViewController(withIdentifier: "RecordAudioVC") as! RecordAudioVC
        
        self.present(recordAudioVC, animated: true, completion: nil)
    }
    @IBAction func actionOnPlayAudio(_ sender: UIButton) {
        
        if(FileManager.default.fileExists(atPath: getAudioFileURL().path))
        {
            if(audioPlayer != nil)
            {
                if(audioPlayer.isPlaying)
                {
                    audioPlayer.stop()
                    btnPlayAudio.setImage(UIImage(named: "play"), for: .normal)
                }
                else
                {
                    do
                    {
                        self.audioPlayer = try AVAudioPlayer(contentsOf: getAudioFileURL())
                        self.audioPlayer.prepareToPlay()
                        self.audioPlayer.delegate = self
                        self.audioPlayer.play()
                        btnPlayAudio.setImage(UIImage(named: "pause"), for: .normal)
                    }
                    catch  {
                        
                    }
                }
            }
            else{
                do
                {
                    self.audioPlayer = try AVAudioPlayer(contentsOf: getAudioFileURL())
                    self.audioPlayer.prepareToPlay()
                    self.audioPlayer.delegate = self
                    self.audioPlayer.play()
                    btnPlayAudio.setImage(UIImage(named: "pause"), for: .normal)
                }
                catch  {
                    
                }
            }
        }
    }
    
    @IBAction func actionOnSeverity(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoPopViewWithArray(sender: sender , aryData: arraySeverity, strTitle: "")
    }
    
    func openCameraOrGallery() {
        
        let alertController = UIAlertController(title: "Message", message: "Choose Image from", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
                
                switch cameraAuthorizationStatus {
                case .denied:
                    self.openSettingsForCameraAndPhoto(str: "Camera permission not allowed, please go to settigs and allow.")
                    break
                case .authorized:
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                    break
                case .restricted:
                    self.openSettingsForCameraAndPhoto(str: "Camera permission not allowed, please go to settigs and allow.")
                    break
                    
                case .notDetermined:
                    // Prompting user for the permission to use the camera.
                    AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                        if granted {
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            
                            self.present(self.imagePicker, animated: true, completion: nil)
                        } else {
                            self.openSettingsForCameraAndPhoto(str: "Camera permission not allowed, please go to settigs and allow.")
                        }
                    }
                }
            }
        }))
        alertController.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
               
                switch photoAuthorizationStatus {
                case .denied:
                    self.openSettingsForCameraAndPhoto(str: "Gallery permission not allowed, please go to settigs and allow.")
                    break
                case .authorized: 
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: {
                        self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
                    })
                    break
                case .restricted:
                    self.openSettingsForCameraAndPhoto(str: "Gallery permission not allowed, please go to settigs and allow.")
                    break
                    
                case .notDetermined:
                    // Prompting user for the permission to use the camera.
                    
                    PHPhotoLibrary.requestAuthorization { (status) in
                        
                        if status == PHAuthorizationStatus.authorized
                        {
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .photoLibrary
                            self.imagePicker.allowsEditing = false
                            
                            self.present(self.imagePicker, animated: true, completion: {
                                self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
                            })
                        }
                        else
                        {
                            self.openSettingsForCameraAndPhoto(str: "Gallery permission not allowed, please go to settigs and allow.")
                        }
                    }
                default : break
                }
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openSettingsForCameraAndPhoto(str: String)
    {
        let alertController = UIAlertController (title:"Alert" , message: str, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func openSettingsForMicrphone()
    {
        let alertController = UIAlertController (title: "Alert", message: "Audio permission not allowed, please go to settigs and allow.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: PopUpView method
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleSelectionOnEditOpportunity = self
        vc.aryTBL = arraySeverity
        self.present(vc, animated: false, completion: {})
        
    }
    
      // MARK: ImagePicker controller delegate
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {            
            let compressedImage = compressImage(image: image)
            print(compressedImage)
          
            if(arrayImages.count < 10)
            {
            arrayImages.add(compressedImage)
            }
        }
        self.dismiss(animated: true, completion: nil)
       
        if(arrayImages.count == 10)
        {
            btnAddImages.isHidden = true
        }
        else{
            btnAddImages.isHidden = false
        }
        
        self.hghtContCollectionView.constant = 80.0;
        collectionViewImagesServiceIssue.reloadData()
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Image picker did cancel called")
         self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UITextView delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Message..."
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, set
            // the text color to black then set its text to the
            // replacement string
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = text
        }
            
            // For every other case, the text should change with the usual
            // behavior...
        else {
            return true
        }
        
        // ...otherwise return false since the updates have already
        // been made
        return false
    }
    
    // MARK: Web Service
    func serviceComplain(){        
        
        let strEmail = UserDefaults.standard.value(forKey: "email")
    
        let dict = ["CompanyKey":"\(dictWO.value(forKey: "CompanyKey")!)",
                    "AppKey":"\(dictWO.value(forKey: "AppKey")!)",
                    "AccountNo":"\(dictWO.value(forKey: "AccountNo")!)",
                    "WorkOrderNo":"\(dictWO.value(forKey: "WorkOrderNo")!)",
                    "Message":"\(txtviewMsg.text!)",
                    "FromEmail":strEmail,
                    "Severity" :"\((btnSeverity.currentTitle)!)"]
        
        var dictJson = NSDictionary()
        var jsondata = Data()
        do{
            jsondata = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
          
            let jsonString = String(data: jsondata, encoding: String.Encoding.utf8)
            
            dictJson = ["lifeStyleServiceComplaintDc" : jsonString!]
        }
        catch
        {
            print("error")
        }
      
        var audiodata = NSData()
      
        if(FileManager.default.fileExists(atPath: getAudioFileURL().path))
        {
            do{
                audiodata =  try Data(contentsOf: (getAudioFileURL())) as NSData
            }
            catch
            {
                
            }
        }
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
    
        if(arrayImages.count == 0)
        {
          arrayImages = NSMutableArray()
        }
      
        WebService.callAPIWithMultiImageAndAudio(parameter: dictJson, url: URL_ServiceComplaint, imageArray: arrayImages, audioData: audiodata) { (response, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            
            if(status == "success")
            {
                if(containsKey(dict: response) ==  true)// error has occurred
                {
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: "\(response.value(forKey: "Message")!)", viewcontrol: self)
                }
                else
                {
                    if("\(response.value(forKey: "Result")!)" == "true" || "\(response.value(forKey: "Result")!)" == "True" || "\(response.value(forKey: "Result")!)" == "TRUE" || "\(response.value(forKey: "Result")!)" == "1")
                    {
                        let alertController = UIAlertController(title: "Message", message: "Your complaint submitted successfully", preferredStyle: .alert)
                        
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                            
                            deleteAudioFile()
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        
                        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
            }
            
            print(response)
            print(status)
        }

    }
    
    func serviceRequest(){
        
         let strEmail = UserDefaults.standard.value(forKey: "email")
        
        let dict = [
            "AppKey":"Pestream",
            "Message":"\(txtviewMsg.text!)",
            "FromEmail":strEmail,
            "Severity" :"\((btnSeverity.currentTitle)!)"]
        
        
        var dictJson = NSDictionary()
        var jsondata = Data()
        do{
            jsondata = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

            let jsonString = String(data: jsondata, encoding: String.Encoding.utf8)

            dictJson = ["lifeStyleServiceRequestDc" : jsonString!]
        }
        catch
        {
            print("error")
        }//
        
        
        var audiodata = NSData()
        
        if(FileManager.default.fileExists(atPath: getAudioFileURL().path))
        {
            do{
                audiodata =  try Data(contentsOf: (getAudioFileURL())) as NSData
            }
            catch
            {
                
            }
        }
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        if(arrayImages.count == 0)
        {
            arrayImages = NSMutableArray()
        }
        
        WebService.callAPIWithMultiImageAndAudio(parameter: dictJson, url: URL_ServiceRequestForMobile, imageArray: arrayImages, audioData: audiodata) { (response, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            
            if(status == "success")
            {
                if(containsKey(dict: response) ==  true)// error has occurred
                {
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: "\(response.value(forKey: "Message")!)", viewcontrol: self)
                }
                else
                {
                    if("\(response.value(forKey: "Result")!)" == "true" || "\(response.value(forKey: "Result")!)" == "True" || "\(response.value(forKey: "Result")!)" == "TRUE" || "\(response.value(forKey: "Result")!)" == "1")
                    {
                        
                        let alertController = UIAlertController(title: "Message", message: "Your Service Request Submitted Successfully", preferredStyle: .alert)
                        
                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                            
                            deleteAudioFile()
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
            }
            
            print(response)
            print(status)
        }
        
        
        
        
        //        WebService.callAPIBYPOST_string(parameter: dict as NSDictionary, url: URL_ServiceComplaint) { (response, status) in
        //
        //            FTIndicator.dismissProgress()
        //            if(status == "success")
        //            {
        //                if(("\(response.value(forKey: "data")!)" == "true") || ("\(response.value(forKey: "data")!)" == "True") || ("\(response.value(forKey: "data")!)" == "TRUE"))
        //                {
        //                    let alertController = UIAlertController(title: "Message", message: "Your complaint submitted successfully", preferredStyle: .alert)
        //
        //                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
        //
        //                        self.navigationController?.popViewController(animated: true)
        //                    }))
        //                    self.present(alertController, animated: true, completion: nil)
        //                }
        //                else{
        //                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
        //                }
        //            }
        //            else{
        //                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
        //            }
        //        }
    }
    
    private func makeEffectView() {
     let effect: UIBlurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
     effectView = UIVisualEffectView(effect: effect)
     effectView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height)
     self.view.addSubview(effectView)
        
        
     }
     
     private func removeEffectView() {
     self.effectView.removeFromSuperview()
     
     }
}

extension SurveyComplainVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImages.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImageServiceIssue", for: indexPath) as! CellImageServiceIssue
        
        cell.imgview.image = arrayImages.object(at: indexPath.row) as? UIImage
        cell.imgview.layer.cornerRadius = cell.imgview.frame.size.width/2
        cell.imgview.layer.borderWidth = 2.0
        cell.imgview.layer.borderColor = UIColor.white.cgColor
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: "Options", message: "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Remove Picture", style: .default, handler: { (alertAction) in
            
            self.arrayImages.removeObject(at: indexPath.row)
            self.btnAddImages.isHidden = false
            if(self.arrayImages.count > 0)
            {
                self.collectionViewImagesServiceIssue.reloadData()
            }
            else{
                // make collection view height zero
                self.hghtContCollectionView.constant = 0.0;
            }
                        
        }))
        
        alertController.addAction(UIAlertAction(title: "Preview", style: .default, handler: { (alertAction) in

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let imgPreviewVC = storyboard.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
            imgPreviewVC.imgView = self.arrayImages.object(at: indexPath.row) as! UIImage
            
            self.present(imgPreviewVC, animated: true, completion: nil)
          
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
}

extension SurveyComplainVC:AVAudioPlayerDelegate{
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
       
        btnPlayAudio.setImage(UIImage(named: "play"), for: .normal)
    }
}

extension SurveyComplainVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(str : String) {
        self.view.endEditing(true)
        btnSeverity.setTitle(str, for: .normal)
        strSeverity = str
    }
}
