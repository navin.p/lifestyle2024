//
//  DashboardVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 28/02/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import FirebaseAuth
import Alamofire
import SafariServices

//MARK: ---------------TableViewCell Class-----------------

class CellUpcomingActivities: UITableViewCell{
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var btnEmployeeName: UIButton!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var btnOrderNo: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnReschedule: UIButton!
    @IBOutlet weak var imgViewCompanyLogo: UIImageView!
    
    @IBOutlet weak var constWidthConfirmBtn: NSLayoutConstraint!
    
    @IBOutlet weak var constLeadingConfirmBtn: NSLayoutConstraint!
    
}
class CellCompletedActivities: UITableViewCell{
    
    @IBOutlet weak var lblCompanyName: UILabel!    
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var btnEmployeeName: UIButton!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var btnSurvey: UIButton!
    
    @IBOutlet weak var btnDocuments: UIButton!
    
    @IBOutlet weak var btnServiceComplain: UIButton!
    
    @IBOutlet weak var viewVertical1: UIView!
    
    @IBOutlet weak var viewVertical2: UIView!
    
    @IBOutlet weak var imgViewCompanyLogo: UIImageView!
    
    @IBOutlet weak var btnOrderNo: UIButton!
    
}
class CellPendingActivities: UITableViewCell{
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblLeadNo: UILabel!
    @IBOutlet weak var lblDocType: UILabel!
    @IBOutlet weak var btnSignatureLink: UIButton!
    @IBOutlet weak var imgViewCompanyLogo: UIImageView!
}

//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    
    func refreshDrawerScreen(strType : String ,tag : Int)
}

extension DashboardVC: DrawerScreenDelegate {
    
    func refreshDrawerScreen(strType: String, tag: Int) {
        
        if(strType == "My Tasks"){
            performSegue(withIdentifier: "TaskVC", sender: nil)
        }
        else if(strType == "Transactions")
        {
            performSegue(withIdentifier: "TransactionsVC", sender: nil)
        }
        else if(strType == "Documents")
        {
            performSegue(withIdentifier: "DocumentsVC", sender: nil)
        }
        else if(strType == "Service")//Work Orders renamed Service
        {
            performSegue(withIdentifier: "WorkOrdersVC", sender: nil)
        }
        else if(strType == "Change Password")
        {
            performSegue(withIdentifier: "ChangePasswordVC", sender: nil)
        }
        else if(strType == "Logout")
        {
            if Auth.auth().currentUser != nil {
                do {
                    try Auth.auth().signOut()
                    for item in (self.navigationController?.viewControllers)!
                    {
                        if(item is LoginVC)
                        {
                            deleteAllSavedDataFromUserDefaults()
                            self.navigationController?.popToViewController(item, animated: false)
                            break
                        }
                    }
                    
                } catch let error as NSError {
                    
                    print(error.localizedDescription)
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Some error occurred", viewcontrol: self)
                }
            }
        }
    }    
}


// MARK: - DashboardVC Class
class DashboardVC: UIViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tvDashboard: UITableView!
    var dictDashboardData = NSDictionary()
    var dictWODetails = NSDictionary()
    var strActivityType = "all"
    private  let  refreshControl = UIRefreshControl()
    
    // MARK: - View's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // tvDashboard.estimatedRowHeight = 80.0
        tvDashboard.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            tvDashboard.refreshControl = refreshControl
        } else {
            tvDashboard.addSubview(refreshControl)
        }
        
        refreshControl.tintColor = appThemeColor
        //  refreshControl.addTarget(self, action: #selector(refreshDashboard(_:)), for: .valueChanged)
        refreshControl.addTarget(self, action: #selector(refreshDashboard), for: .valueChanged)
        
         if(isInternetAvailable())
         {
             getDashboard()
        }
         else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
       
    }
    
    
    // MARK: - UIButton action
    
    @IBAction func actionOpenMenu(_ sender: UIButton) {
        
        self.btnMenu.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.btnMenu.transform = .identity
            }, completion: nil)
        
        let vc: DrawerVC = self.storyboard!.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDrawerView = self
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actionOnServiceRequest(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                               let SCVC = storyboard.instantiateViewController(withIdentifier: "TakeImageVC") as! TakeImageVC
//                               self.navigationController?.pushViewController(SCVC, animated: true)
                                self.goToServiceComplaintController(dictWO: NSDictionary(), strTitle: "Service Request")
                                
                            })
        })
    }
    
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                self.performSegue(withIdentifier: "FilterVC", sender: nil)
                            })
        })
        
    }
    
    
    @IBAction func actionOnDocument(_ sender: Any) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                               self.performSegue(withIdentifier: "DocumentsVC", sender: nil)
                            })
        })
       
    }
    
    @IBAction func actionOnTransaction(_ sender: Any) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                self.performSegue(withIdentifier: "TransactionsVC", sender: nil)

                            })
        })
    }
    
    @IBAction func actionOnPastService(_ sender: Any) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                self.performSegue(withIdentifier: "WorkOrdersVC", sender: nil)
                                
                            })
        })
        

    }
    
    @IBAction func actionOnTask(_ sender: Any) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                self.performSegue(withIdentifier: "TaskVC", sender: nil)
                                
                            })
        })
        
    }
    
    // MARK: tableview cell button action
    @objc func actionOnEmployeeName(sender:UIButton!)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: tvDashboard)
        let indexPath = tvDashboard.indexPathForRow(at:buttonPosition)
        if(indexPath?.section == 0)
        {
          dictWODetails = (dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        }
        else if(indexPath?.section == 1)
        {
          dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        }
        
        if("\(dictWODetails.value(forKey: "TechnicianName")!)" == "<null>" || ("\(dictWODetails.value(forKey: "TechnicianName")!)").count == 0)
        {
            // dont send to employee detail controller
        }
        else
        {
           performSegue(withIdentifier: "EmployeeProfileVC", sender: nil)
        }
       
    }
    
    @objc func actionOnOrderNumber(sender:UIButton!){
        
        
        let position: CGPoint = sender.convert(CGPoint.zero, to: tvDashboard)

        if let indexPath = tvDashboard.indexPathForRow(at: position)
        {
            let section = indexPath.section
            let row = indexPath.row
        
            print(section)
            print(row)
        if(section == 0)
        {
            dictWODetails = (dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
            
            if("\(dictWODetails.value(forKey: "WorkOrderNo")!)" == "<null>" || ("\(dictWODetails.value(forKey: "WorkOrderNo")!)").count == 0)
            {
                // dont send to WO detail controller
            }
            else
            {
                performSegue(withIdentifier: "WODetailsVC", sender: nil)
            }
        }
        else
        {
            dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
            
            if("\(dictWODetails.value(forKey: "WorkOrderNo")!)" == "<null>" || ("\(dictWODetails.value(forKey: "WorkOrderNo")!)").count == 0)
            {
                // dont send to WO detail controller
            }
            else
            {
                performSegue(withIdentifier: "WODetailsVC", sender: nil)
            }
          }
        }
    }
    
    @objc func actionOnConfirm(sender:UIButton!){
        
        if(isInternetAvailable()){
            let alertController = UIAlertController(title: "Message", message: "Are you sure you want to confirm?", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: { (alertAction) in
                
                let dict = (self.dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
                print(dict)
                
                self.callConfirmAPI(dictParam: dict)
                
            }))
            alertController.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (alertAction) in
                
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }        
    }
    
    @objc func actionOnReschedule(sender:UIButton!){
        
        dictWODetails = (dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        
        performSegue(withIdentifier: "RescheduleWorkOrderVC", sender: nil)
    }
    
    @objc private func refreshDashboard(_sender:Any)
    {
        if(isInternetAvailable()){
            getDashboard()
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    @objc private func actionOnViewMore(sender:UIButton!)
    {
        if(sender.tag == 2)
        {
            performSegue(withIdentifier: "DocumentsVC", sender: nil)
        }
        else{
            performSegue(withIdentifier: "WorkOrdersVC", sender: nil)
        }
    }
    
    @objc private func actionOnSignature(sender:UIButton)
    {
        let dictTemp = (dictDashboardData.value(forKey: "RecentPendingAgreementDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        
        let signLink = "\(dictTemp.value(forKey: "SignatureLinkforAgree")!)"
        
        let url = NSURL(string: signLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
        
        if(url != nil)
        {
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
    }
    
    //
    @objc private func actionOnServiceComplain(sender:UIButton)
    {
        dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        goToServiceComplaintController(dictWO: dictWODetails, strTitle: "Service Issue")
    }
 
    @objc private func actionOnSurvey(sender:UIButton)
    {
         dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        
       if(dictWODetails.value(forKey: "SurveyURL") != nil && "\(dictWODetails.value(forKey: "SurveyURL")!)" != "<null>")
       {
        
         let strURL = dictWODetails.value(forKey: "SurveyURL")
           
            let url = NSURL(string: (strURL as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
            
            if(url != nil)
            {
                let safariVC = SFSafariViewController(url: url!)
                safariVC.delegate = self
                self.present(safariVC, animated: true, completion: nil)
            }
        }
       else{
        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @objc private func actionOnReview(sender:UIButton)
    {
        
    }
    @objc private func actionOnPayBill(sender:UIButton)
    {
        print("Payment button clicked")
         dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        print(dictWODetails)
        generatePaymentToken()
        
    }
    @objc private func actionOnDocuments(sender:UIButton)
    {
        dictWODetails = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: sender.tag) as! NSDictionary
        print(dictWODetails)
       performSegue(withIdentifier: "WODocumentsVC", sender: nil)
    }
    
    // MARK: - Segue Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "WODetailsVC")
        {
            let vc = segue.destination as! WODetailsVC
            vc.dictWODetails = dictWODetails
        }
        else if(segue.identifier == "EmployeeProfileVC")
        {
            let vc = segue.destination as! EmployeeProfileVC
            vc.dictData = dictWODetails
        }
        else if(segue.identifier == "RescheduleWorkOrderVC")
        {
            let vc = segue.destination as! RescheduleWorkOrderVC
            vc.dictWO = dictWODetails
        }
        else if(segue.identifier == "WODocumentsVC"){
           
            let vc = segue.destination as! WODocumentsVC
            vc.dictWO = dictWODetails
        }
    }
    
    func goToServiceComplaintController(dictWO : NSDictionary, strTitle: NSString)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCVC = storyboard.instantiateViewController(withIdentifier: "SurveyComplainVC") as! SurveyComplainVC
        SCVC.dictWO  = dictWO
        SCVC.strTitle = strTitle
        
        self.navigationController?.pushViewController(SCVC, animated: true)
    }
    
    // MARK: - Web service to get dashboard data
    func getDashboard()
    {
        let arrayAccounts = getUserAccounts()
        if(arrayAccounts.count > 0)
        {
            print(arrayAccounts)
            
           /* let dictJson = ["CompanyKey":"automation", "AppKey":"Pestream", "AccountNo":"D029771"]

            var parameters = [[String:AnyObject]]()

            parameters.append(dictJson as [String : AnyObject])*/
            
            
            //
            if let url = NSURL(string:URL_DashboardDetails)//URL_DashboardDetails
            {
                
                var request = URLRequest(url: url as URL)
                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                
               // request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
                
                 request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
          
//                FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                
                customDotLoaderShowOnFull(message: "", controller: self)

                
                Alamofire.request(request)
                    .responseJSON { response in
                        
//                        FTIndicator.dismissProgress()
                        
                        customeDotLoaderRemove()
                        self.refreshControl.endRefreshing()
                        
                        switch response.result {
                        case .success(let responseObject):
                            
                            self.dictDashboardData = responseObject as! NSDictionary
                            
                            if(containsKey(dict: self.dictDashboardData) ==  true)// error has occurred
                            {
                                showAlertWithoutAnyAction(strtitle: "Message", strMessage: "\(self.dictDashboardData.value(forKey: "Message")!)", viewcontrol: self)
                            }
                            else
                            {
                                //    self.dictDashboardData = (responseObject as! NSArray).firstObject as! NSDictionary
                                DispatchQueue.main.async {
                                    self.tvDashboard.reloadData()
                                }
                            }
                            
                            print(responseObject)
                            
                        case .failure(let error):
                            
                            print(error)
                        }
                }
            }
        }
    }
    
    func callConfirmAPI(dictParam:NSDictionary){       
        
        let strUrl = BaseURL+"UpdateWorkOrderFlagToConfirmed?appKey=\(dictParam.value(forKey: "AppKey")!)&companyKey=\(dictParam.value(forKey: "CompanyKey")!)&WorkOrderNo=\(dictParam.value(forKey: "WorkOrderNo")!)"
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)

        WebService.callAPIBYGETWhoseResponseIsString(parameter: NSDictionary(), url: strUrl) { (response, status) in
            
//            FTIndicator.dismissProgress()
            
            customeDotLoaderRemove()
            if(status == "success")
            {
                if(response == "True" || response == "true" || response == "TRUE")
                {
                    print(response)
                    self.getDashboard()
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                }
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                print("some error occurred in confirm api")
            }
        }
    }
 
    func generatePaymentToken()
    {
        let dict = ["AppKey":"\(dictWODetails.value(forKey: "AppKey")!)",
                    "CompanyKey":"\(dictWODetails.value(forKey: "CompanyKey")!)",
                    "AccountNo":"\(dictWODetails.value(forKey: "AccountNo")!)",
                    "EntityType":"Workorder",
                    "EntityNumber":"\(dictWODetails.value(forKey: "WorkOrderNo")!)",
                    "TransactionAmount":"2",//"\(dictWODetails.value(forKey: "TransactionAmount")!)"
                    "ReturnURL":""]
     
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: dict,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
        }
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        WebService.callAPIBYPOST(parameter: dict as NSDictionary, url: "http://api.lifestyle2024.com//api/Customer/GeneratePaymentToken") { (response, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
           
            print(status)
            print(response)
            
            if(status == "success")
            {
                if(("\(response.value(forKey: "data")!)").count > 0)
                {
                    //"http://tcr.stagingsoftware.com/payment/GeneralPayment/Index?tokenString=",
                    
                    let strURL = "http://pestream.com/payment/GeneralPayment/Index?tokenString=" + "\(response.value(forKey: "data")!)"
                    
                    let url = NSURL(string: (strURL ).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
                    
                    if(url != nil)
                    {
                        let safariVC = SFSafariViewController(url: url!)
                        safariVC.delegate = self
                        self.present(safariVC, animated: true, completion: nil)
                    }
                }
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                print("some error occurred in confirm api")
            }
            
        }        
    }
}

extension DashboardVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // agar all selected h to saari activities ke sections(upcoming,pending,completed)  dikhenge nh to sirf ek hi section dikhega according to strActivityType(upcoming ya fir completed)
        if(strActivityType == "all"){
            return 3
        }
        return 1
    }
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //
    //        if(strActivityType == "all")
    //        {
    //            if(section == 0)
    //            {
    //                return "Upcoming Activities"
    //            }
    //            else if(section == 1)
    //            {
    //                return "Completed Services"
    //            }
    //            else
    //            {
    //                return "Pending Services"
    //            }
    //        }
    //        else if(strActivityType == "completed")
    //        {
    //            return "Completed Services"
    //        }
    //
    //        return "Pending Services"
    //    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = CardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
        headerView.backgroundColor = hexStringToUIColor(hex: "D0D0D0")
        
        let headerLbl = UILabel(frame: CGRect(x: 20, y: 0, width: 280, height: 40))
        headerLbl.font = UIFont(name: "Lato", size: 18.0)
        //headerLbl.textColor = UIColor.white
        if(strActivityType == "all")
        {
            if(section == 0)
            {
                headerLbl.text = "Upcoming Services"
            }
            else if(section == 1)
            {
                headerLbl.text = "Completed Services"
            }
            else
            {
                headerLbl.text = "Pending Agreements"
            }
        }
        else if(strActivityType == "upcoming")
        {
            headerLbl.text = "Upcoming Services"
        }
        else
        {
            headerLbl.text = "Completed Services"
        }
        
        headerView.addSubview(headerLbl)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        footerView.backgroundColor = UIColor.white
//            hexStringToUIColor(hex: "D0D0D0")

         let footerbtn = UIButton(frame: CGRect(x: self.view.frame.size.width-100, y: 5, width: 100, height: 21))

        footerbtn.titleLabel?.font = UIFont(name: "Lato", size: 14.0)
        
        footerbtn.setTitle("View More...", for: .normal)
        footerbtn.titleLabel?.textAlignment = NSTextAlignment.center
        footerbtn.setTitleColor(UIColor.black, for: .normal)
        
        footerbtn.tag = section
        footerbtn.addTarget(self, action: #selector(actionOnViewMore), for: .touchUpInside)
        footerView.addSubview(footerbtn)

        return footerView

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(dictDashboardData.count > 0)
        {
            if(strActivityType == "all")
            {
                if(section == 0)
                {
                    return (dictDashboardData.value(forKey: "UpcomingWorkorderDcs")as! NSArray).count
                }
                else if(section == 1)
                {
                    return (dictDashboardData.value(forKey: "RecentWorkorderDcs")as! NSArray).count
                }
                else
                {
                    return (dictDashboardData.value(forKey: "RecentPendingAgreementDcs")as! NSArray).count
                }
            }
            else if(strActivityType == "upcoming")
            {
                return (dictDashboardData.value(forKey: "UpcomingWorkorderDcs")as! NSArray).count
            }
            return (dictDashboardData.value(forKey: "RecentWorkorderDcs")as! NSArray).count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellUpcoming = tableView.dequeueReusableCell(withIdentifier: "CellUpcomingActivities", for: indexPath) as! CellUpcomingActivities
        
        let cellPending = tableView.dequeueReusableCell(withIdentifier: "CellPendingActivities", for: indexPath) as! CellPendingActivities
        
        let cellCompleted = tableView.dequeueReusableCell(withIdentifier: "CellCompletedActivities", for: indexPath) as! CellCompletedActivities
        
            if(indexPath.section == 0)// upcoming
            {
                let dictData = (dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray).object(at: indexPath.row) as! NSDictionary
                
                let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
                
                SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                    
                }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                    
                    if image != nil {
                        
                        addAnimationOnView(view: cellUpcoming.imgViewCompanyLogo)
                        cellUpcoming.imgViewCompanyLogo.image = image
                    }                    
                })
                
                cellUpcoming.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
                cellUpcoming.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

                if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
                {
                  cellUpcoming.btnEmployeeName.setTitle("", for: .normal)
                }
                else
                {
                  cellUpcoming.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
                }
                
                cellUpcoming.btnEmployeeName.tag = indexPath.row
                cellUpcoming.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
                
                if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
                {
                    cellUpcoming.lblDateTime.text = ""
                }
                else
                {
                      cellUpcoming.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
                }
                
                cellUpcoming.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
                
                cellUpcoming.btnOrderNo.tag = indexPath.row
                cellUpcoming.btnOrderNo.addTarget(self, action: #selector(actionOnOrderNumber), for: .touchUpInside)
                
                cellUpcoming.btnConfirm.tag = indexPath.row
                cellUpcoming.btnConfirm.layer.cornerRadius =  cellUpcoming.btnConfirm.frame.size.height/2
                
                if("\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "true" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "True" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "TRUE" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "1")
                {
                    cellUpcoming.btnConfirm.setTitle("Confirmed", for: .normal)
                    cellUpcoming.btnConfirm.isUserInteractionEnabled = false
                    cellUpcoming.btnConfirm.alpha = 0.5
                }
                else
                {
                    cellUpcoming.btnConfirm.setTitle("Confirm", for: .normal)
                    
                    cellUpcoming.btnConfirm.isUserInteractionEnabled = true
                    
                    cellUpcoming.btnConfirm.addTarget(self, action: #selector(actionOnConfirm), for: .touchUpInside)
                    cellUpcoming.btnConfirm.alpha = 1
                }
                
                cellUpcoming.btnReschedule.layer.cornerRadius =  cellUpcoming.btnReschedule.frame.size.height/2
                cellUpcoming.btnReschedule.tag = indexPath.row
                cellUpcoming.btnReschedule.addTarget(self, action: #selector(actionOnReschedule), for: .touchUpInside)
                
                if("\(dictData.value(forKey: "WOOnMyWay")!)" == "true" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "True" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "TRUE" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "1" && "\(dictData.value(forKey: "TimeIn")!)" == "<null>")
                {
                    cellUpcoming.btnConfirm.isHidden = true
                    cellUpcoming.btnReschedule.isHidden = true
                    cellUpcoming.contentView.backgroundColor = UIColor.yellow
                }
                else
                {
                    cellUpcoming.btnConfirm.isHidden = false
                    cellUpcoming.btnReschedule.isHidden = false
                    cellUpcoming.contentView.backgroundColor = UIColor.white
                }
                
                return cellUpcoming
            }
            else if(indexPath.section == 1)// completed
            {
                let dictData = (dictDashboardData.value(forKey: "RecentWorkorderDcs") as! NSArray).object(at: indexPath.row) as! NSDictionary
                
                let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
                SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                    
                }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                    
                    if image != nil {
                       
                        addAnimationOnView(view: cellCompleted.imgViewCompanyLogo)
                      
                        cellCompleted.imgViewCompanyLogo.image = image
                    }
                    
                })
                
                cellCompleted.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
                
                cellCompleted.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

                 if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
                 {
                    cellCompleted.btnEmployeeName.setTitle("", for: .normal)
                 }
                 else
                 {
                    cellCompleted.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
                 }
                
                cellCompleted.btnEmployeeName.tag = indexPath.row
                
                cellCompleted.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
                
                if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
                {
                    cellCompleted.lblDateTime.text = ""
                }
                else
                {
                      cellCompleted.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
                }
                
                cellCompleted.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
                
                cellCompleted.btnOrderNo.tag = indexPath.row
                cellCompleted.btnOrderNo.addTarget(self, action: #selector(actionOnOrderNumber), for: .touchUpInside)
                
                // cellCompleted.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
                
               cellCompleted.btnSurvey.layer.cornerRadius = cellCompleted.btnSurvey.frame.size.height/2
               cellCompleted.btnSurvey.tag = indexPath.row
               cellCompleted.btnSurvey.addTarget(self, action: #selector(actionOnSurvey), for: .touchUpInside)
                
               cellCompleted.btnPay.layer.cornerRadius = cellCompleted.btnPay.frame.size.height/2
               cellCompleted.btnPay.tag = indexPath.row
               cellCompleted.btnPay.addTarget(self, action: #selector(actionOnPayBill), for: .touchUpInside)
                
               cellCompleted.btnServiceComplain.layer.cornerRadius = cellCompleted.btnServiceComplain.frame.size.height/2
               cellCompleted.btnServiceComplain.tag = indexPath.row
               cellCompleted.btnServiceComplain.addTarget(self, action: #selector(actionOnServiceComplain), for: .touchUpInside)
                
                cellCompleted.btnDocuments.layer.cornerRadius = cellCompleted.btnDocuments.frame.size.height/2
                cellCompleted.btnDocuments.tag = indexPath.row
                cellCompleted.btnDocuments.addTarget(self, action: #selector(actionOnDocuments), for: .touchUpInside)
                
                
                if("\(dictData.value(forKey: "InvoiceStatus")!)" == "Closed" || "\(dictData.value(forKey: "InvoiceStatus")!)" == "closed")
                {
                    cellCompleted.btnPay.isHidden = false
                    cellCompleted.btnPay.isUserInteractionEnabled = false
                    cellCompleted.btnPay.backgroundColor = greenColor
                    cellCompleted.btnPay.setTitle("Paid", for: .normal)
                }
                else
                {
                    if let transactionAmount = dictData.value(forKey: "TransactionAmount") as? NSNumber {
                        let amt = transactionAmount.floatValue
                        
                        if(amt > 0.0)
                        {
                            cellCompleted.btnPay.isHidden = false
                            cellCompleted.btnPay.isUserInteractionEnabled = true
                            cellCompleted.btnPay.backgroundColor = UIColor.red
                            cellCompleted.btnPay.setTitle("Pay Bill", for: .normal)
                        }
                        else
                        {
                            cellCompleted.btnPay.isHidden = true
                        }
                    }
                    
                }
               
                
                return cellCompleted
            }
            else // pending
            {
                let dictData = (dictDashboardData.value(forKey: "RecentPendingAgreementDcs") as! NSArray).object(at: indexPath.row) as! NSDictionary
              
                
                cellPending.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
                cellPending.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"
                cellPending.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ScheduleStartDate")!)", formet: "yyyy-MM-dd'T'HH:mm:ss'Z'", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
                cellPending.lblLeadNo.text = "Lead#: \(dictData.value(forKey: "LeadNumber")!)"
                cellPending.lblDocType.text = "\(dictData.value(forKey: "DocType")!)"
               
                
                let url = URL(string: "\(dictData.value(forKey: "CompanyLogoPath")!)")
                SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                    
                }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                    
                    if image != nil {
                        
                        addAnimationOnView(view: cellPending.imgViewCompanyLogo)
                        
                        cellPending.imgViewCompanyLogo.image = image
                    }
                    
                })
                
                cellPending.btnSignatureLink.tag = indexPath.row
                cellPending.btnSignatureLink.layer.cornerRadius = cellPending.btnSignatureLink.frame.size.height/2
                cellPending.btnSignatureLink.addTarget(self, action: #selector(actionOnSignature), for: .touchUpInside)
              
                if("\(dictData.value(forKey:"IsAgreementSigned")!)" == "true" || "\(dictData.value(forKey: "IsAgreementSigned")!)" == "True" || "\(dictData.value(forKey: "IsAgreementSigned")!)" == "TRUE" || "\(dictData.value(forKey: "IsAgreementSigned")!)" == "1")
                {
                    cellPending.btnSignatureLink.isHidden = true
                }
                else{
                    cellPending.btnSignatureLink.isHidden = false
                }
                
                return cellPending
            }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.section == 0)
        {
            let array = dictDashboardData.value(forKey: "UpcomingWorkorderDcs") as! NSArray
            let dict = array.object(at: indexPath.row) as! NSDictionary
           
            if("\(dict.value(forKey: "WOOnMyWay")!)" == "true" || "\(dict.value(forKey: "WOOnMyWay")!)" == "True" || "\(dict.value(forKey: "WOOnMyWay")!)" == "TRUE" || "\(dict.value(forKey: "WOOnMyWay")!)" == "1" && "\(dict.value(forKey: "TimeIn")!)" == "<null>")
            {
                // go to track view controller
                
                let trackVC = storyboard?.instantiateViewController(withIdentifier: "TrackVC") as! TrackVC
                trackVC.dictWODetails = dict
              //  self.navigationController?.pushViewController(trackVC, animated: true)
                // tracking next build me karna hai, bs is line ko uncomment karna hai
            }
            else
            {
               // do nothing
            }
        }
    }
}
extension DashboardVC:SFSafariViewControllerDelegate{
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}
