//
//  ImagePreviewVC.swift
//  Lifestyle
//
//  Created by Rakesh Jain on 17/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class ImagePreviewVC: UIViewController,UIScrollViewDelegate {

    // outlets
    @IBOutlet weak var scrollViewPreview: UIScrollView!
    @IBOutlet weak var imgViewPreview: UIImageView!
    
    // variables
    
    var imgView = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imgViewPreview.image = imgView
    }

    @IBAction func actionOnBack(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
       return imgViewPreview
    }
}
