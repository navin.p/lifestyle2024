//
//  DocumentsVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 19/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices
// tableview cell
class CellPendingAgreements: UITableViewCell{
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLeadNo: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSignature: UIButton!
    
    @IBOutlet weak var imgViewCompanyLogo: UIImageView!
}
class CellOtherDocuments: UITableViewCell{
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var btnTitle: UIButton!    
    @IBOutlet weak var imgviewCompanyLogo: UIImageView!
    
}

class DocumentsVC: UIViewController {

    // outlets
    @IBOutlet weak var constLeadingView: NSLayoutConstraint!
    @IBOutlet weak var btnPendingAgreement: UIButton!
    @IBOutlet weak var btnOtherDoc: UIButton!    
    @IBOutlet weak var tvDocs: UITableView!
    
    // variables
    var arrayPendingAgreements = NSArray()
    var arrayDocs = NSArray()
    var isPendingAgreementSelected = Bool()
    var strSignURL = ""
    
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isPendingAgreementSelected = true
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if(isInternetAvailable()){
            getPendingAgreements()
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
    // MARK: UIButton action

    @IBAction func actionOnPendingAgreements(_ sender: UIButton) {
        
        isPendingAgreementSelected = true
        constLeadingView.constant = 5
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if(arrayPendingAgreements.count > 0)
        {
            tvDocs.reloadData()
        }
        else
        {
            if(isInternetAvailable()){
                getPendingAgreements()
            }
            else
            {
              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
    }
    
    @IBAction func actionOnOtherDoc(_ sender: UIButton) {
        
        isPendingAgreementSelected = false
        
        constLeadingView.constant = btnOtherDoc.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if(arrayDocs.count > 0)
        {
            tvDocs.reloadData()
        }
        else
        {
            if(isInternetAvailable()){
                getAccountDocuments()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
    @objc func actionOnSignature(_ sender: UIButton!){
        
        strSignURL = "\((arrayPendingAgreements.object(at: sender.tag) as! NSDictionary).value(forKey: "SignatureLinkforAgree")!)"
     
        if(strSignURL.count > 0)
        {
            let safariVC = SFSafariViewController(url: NSURL(string: strSignURL)! as URL)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
       
       
     
       // performSegue(withIdentifier: "WebVC", sender: nil)
        
    }
    @objc func actionOnTitle(_ sender: UIButton!){
        
       let strDocURL = "\((arrayDocs.object(at: sender.tag) as! NSDictionary).value(forKey: "Path")!)"
      
        if(strDocURL.count > 0)
        {
            let url = NSURL(string: strDocURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
           
            if(url != nil)
            {
                let safariVC = SFSafariViewController(url: url!)
                safariVC.delegate = self
                self.present(safariVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
//        self.navigationController?.pushViewController(docVC, animated: true)
        
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TransactionsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
        
    }
    
    @IBAction func actionOnPastServices(_ sender: UIButton) {
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is WorkOrdersVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
            self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTask(_ sender: Any) {
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TaskVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    //MARK: Segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "WebVC"){
            
            let vc = segue.destination as! WebVC
            vc.strWebURL = strSignURL
        }
    }

    //MARK: Web Service
    func getPendingAgreements(){        
        
            let arrayAccounts = getUserAccounts()
            if(arrayAccounts.count > 0)
            {
                print(arrayAccounts)
               //
                let dictJson = ["CompanyKey":"automation", "AppKey":"Pestream", "AccountNo":"D029771"]

                var parameters = [[String:AnyObject]]()

                parameters.append(dictJson as [String : AnyObject])
                
                //
                if let url = NSURL(string:"http://sandboxapi.lifestyle2024.com/api/Customer/GetPendingAgreements")//URL_GetPendingAgreements
                {
                    
                    var request = URLRequest(url: url as URL)
                    
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.httpMethod = "POST"
                    
                    request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
                    
                   //  request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
                    
                    
//                    FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                    
                    customDotLoaderShowOnFull(message: "", controller: self)
              
                    Alamofire.request(request)
                        .responseJSON { response in
                            
//                            FTIndicator.dismissProgress()
                            
                            customeDotLoaderRemove()
                            switch response.result {
                            case .success(let responseObject):
                                
                                print(responseObject)
                                
                                if(responseObject is NSDictionary)
                                {
                                    if(containsKey(dict: responseObject as! NSDictionary))
                                    {
                                        
                                      showAlertWithoutAnyAction(strtitle: "Message", strMessage: "\((responseObject as! NSDictionary).value(forKey: "Message")!)", viewcontrol: self)
                                    }
                                }
                                else
                                {
                                    if((responseObject as! NSArray).count > 0){
                                      self.arrayPendingAgreements = responseObject as! NSArray
                                        DispatchQueue.main.async {
                                            self.tvDocs.reloadData()
                                        }
                                    }
                                    else
                                    {
                                        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                                    }
                                }
                                
                                print(responseObject)
                                
                            case .failure(let error):
                                
                                print(error)
                                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                            }
                    }
                }
            }
        }
    func getAccountDocuments(){
        
        //http://service.lifestyle.com/api/Customer/GetPendingAgreements
        
        let arrayAccounts = getUserAccounts()
        if(arrayAccounts.count > 0)
        {
            print(arrayAccounts)
            
//            let dictJson = ["CompanyKey":"dream.service", "AppKey":"Pestream", "AccountNo":"1"]
//
//            var parameters = [[String:AnyObject]]()
//
//            parameters.append(dictJson as [String : AnyObject])
            
            if let url = NSURL(string:URL_GetAccountDocuments){
                
                var request = URLRequest(url: url as URL)
                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                
               // request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])                
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
                
//                FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                
                customDotLoaderShowOnFull(message: "", controller: self)
             
                Alamofire.request(request)
                    .responseJSON { response in
                        
//                        FTIndicator.dismissProgress()
                        customeDotLoaderRemove()
                        switch response.result {
                        case .success(let responseObject):
                            if(responseObject is NSDictionary)
                            {
                                if(containsKey(dict: responseObject as! NSDictionary))
                                {
                                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: "\((responseObject as! NSDictionary).value(forKey: "Message")!)", viewcontrol: self)
                                }
                            }
                            else
                            {
                                if((responseObject as! NSArray).count > 0){
                                   
                                     self.arrayDocs = responseObject as! NSArray
                                    DispatchQueue.main.async {
                                        self.tvDocs.reloadData()
                                    }
                                }
                                else
                                {
                                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                                }
                            }
                            
                            print(responseObject)
                            
                        case .failure(let error):
                            
                            print(error)
                            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                        }
                }
            }
        }
    }
}

extension DocumentsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isPendingAgreementSelected == true){
            return arrayPendingAgreements.count
        }
        return arrayDocs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellPendingAgreements = tableView.dequeueReusableCell(withIdentifier: "CellPendingAgreements", for: indexPath as IndexPath) as! CellPendingAgreements
        
        let cellOtherDocuments = tableView.dequeueReusableCell(withIdentifier: "CellOtherDocuments", for: indexPath as IndexPath) as! CellOtherDocuments
       if(isPendingAgreementSelected)
       {
        let dictdata = arrayPendingAgreements.object(at: indexPath.row) as! NSDictionary
        cellPendingAgreements.lblCompanyName.text = "\(dictdata.value(forKey: "ProviderCompanyName")!)"
        cellPendingAgreements.lblAccNo.text = "Account # : \(dictdata.value(forKey: "AccountNo")!)"
        cellPendingAgreements.lblLeadNo.text = "Opp # : \(dictdata.value(forKey: "LeadNumber")!)"
        cellPendingAgreements.lblDate.text = dateTimeConvertor(str: "\(dictdata.value(forKey: "ScheduleStartDate")!)", formet: "yyyy-MM-dd'T'HH:mm:ss'Z'", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
       
        //"\(dictdata.value(forKey: "ScheduleStartDate")!)"
        cellPendingAgreements.lblTitle.text = "\(dictdata.value(forKey: "Title")!)"
        
        let url = URL(string: "\(dictdata.value(forKey: "CompanyLogoPath")!)")
       
        SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
            
        }, completed: { (image, error, cacheType, isFinished, imageUrl) in
            
            if image != nil {
                
                addAnimationOnView(view: cellPendingAgreements.imgViewCompanyLogo)
                cellPendingAgreements.imgViewCompanyLogo.image = image
            }
            
        })
        cellPendingAgreements.btnSignature.tag = indexPath.row
        cellPendingAgreements.btnSignature.addTarget(self, action: #selector(actionOnSignature), for: .touchUpInside)
        cellPendingAgreements.btnSignature.layer.cornerRadius = cellPendingAgreements.btnSignature.frame.size.height/2
      
        if("\(dictdata.value(forKey: "IsAgreementSigned")!)" == "true" || "\(dictdata.value(forKey: "IsAgreementSigned")!)" == "True" || "\(dictdata.value(forKey: "IsAgreementSigned")!)" == "true"){
           
            cellPendingAgreements.btnSignature.isHidden = true
            
        }
        else{
            
            cellPendingAgreements.btnSignature.isHidden = false
           // cellPendingAgreements.btnSignature.setTitle("\(dictdata.value(forKey: "SignatureLinkforAgree")!)", for: .normal)
        }
        
        return cellPendingAgreements
    }
       else{
        
         let dictdata = arrayDocs.object(at: indexPath.row) as! NSDictionary
        
        cellOtherDocuments.lblCompanyName.text = "\(dictdata.value(forKey: "ProviderCompanyName")!)"
       
        let url = URL(string: "\(dictdata.value(forKey: "CompanyLogoPath")!)")
        
        SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
            
        }, completed: { (image, error, cacheType, isFinished, imageUrl) in
            
            if image != nil {
                
                addAnimationOnView(view: cellOtherDocuments.imgviewCompanyLogo)
                cellOtherDocuments.imgviewCompanyLogo.image = image
            }
            
        })
        
        cellOtherDocuments.lblAccNo.text = "Account # : \(dictdata.value(forKey: "AccountNo")!)"
         cellOtherDocuments.btnTitle.setTitle("\(dictdata.value(forKey: "Title")!)", for: .normal)
         cellOtherDocuments.btnTitle.tag = indexPath.row
         cellOtherDocuments.btnTitle.addTarget(self, action: #selector(actionOnTitle), for: .touchUpInside)
        return cellOtherDocuments
        }
        
        return cellPendingAgreements
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
}
extension DocumentsVC:SFSafariViewControllerDelegate{
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        controller.dismiss(animated: true, completion: nil)

    }
   
}
