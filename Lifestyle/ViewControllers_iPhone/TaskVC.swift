//
//  TaskVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 28/02/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//



class TaskCell: UITableViewCell {
    
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var lblTaskStatus: UILabel!
    @IBOutlet weak var lblTaskDueDate: UILabel!
    @IBOutlet weak var lblTaskReminderDate: UILabel!    
    @IBOutlet weak var lblTaskDescription: UILabel!
}

import UIKit
import FirebaseDatabase
import FirebaseAuth

class TaskVC: UIViewController {
    
    // outlets
    @IBOutlet weak var tvTask: UITableView!
    @IBOutlet weak var constLeadingView: NSLayoutConstraint!
    
    // variables
    var arrayTask = NSMutableArray()
    var arrayTaskCopy = NSMutableArray()// this array will be used as a filtered array
    var dictTaskForAddTask = NSDictionary()// ye dict addTaskVC per jaegi edit ke liye
    var strTitleForAddTask = "" // ye string value addTaskVC ke title ke liye jaegi
    var selectionType = 1 // 1 for not started, 2 for in progress, 3 for completed
 
    
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tvTask.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllTask()
    }
    
    // MARK: UIButton action

    @IBAction func action_back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is DocumentsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TransactionsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
       
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
//        self.navigationController?.pushViewController(docVC, animated: true)
    }
    
    @IBAction func actionOnPastService(_ sender: UIButton) {
        
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is WorkOrdersVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    @IBAction func actionOnAddTask(_ sender: Any) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                self.strTitleForAddTask = "Add Task"
                                self.performSegue(withIdentifier: "AddTaskVC", sender: nil)
                            })
        })
    }
    
    @IBAction func actionOnNotStarted(_ sender: UIButton) {
        
        var isDataExist = false
        for item in arrayTask{
            let dict = item as! NSDictionary
            if("\(dict.value(forKey: "status")!)" == "Not Started")
            {
                isDataExist = true
                break
            }
        }
        if(isDataExist == true)
        {
            arrayTaskCopy.removeAllObjects()
            for item in arrayTask{
                let dict = item as! NSDictionary
                if("\(dict.value(forKey: "status")!)" == "Not Started")
                {
                    arrayTaskCopy.add(dict)
                }
            }
            self.constLeadingView.constant = 10
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tvTask.reloadData()
        }
        
       
//        if(arrayTaskCopy.count > 0)
//        {
//
//        }
//        else{
//            arrayTaskCopy = arrayTask.mutableCopy() as! NSMutableArray
//        }
    }
    
    @IBAction func actionOnInProgress(_ sender: UIButton) {
        
        var isDataExist = false
        for item in arrayTask{
            let dict = item as! NSDictionary
            if("\(dict.value(forKey: "status")!)" == "In Progress")
            {
                isDataExist = true
                break
            }
        }
        if(isDataExist == true)
        {
            arrayTaskCopy.removeAllObjects()
            for item in arrayTask{
                let dict = item as! NSDictionary
                if("\(dict.value(forKey: "status")!)" == "In Progress")
                {
                    arrayTaskCopy.add(dict)
                }
            }
            selectionType = 2
            self.constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tvTask.reloadData()
        }
        
        
//        if(arrayTaskCopy.count > 0)
//        {
//
//        }
//        else{
//            arrayTaskCopy = arrayTask.mutableCopy() as! NSMutableArray
//        }
    }
    
    @IBAction func actionOnCompleted(_ sender: UIButton) {
       
        var isDataExist = false
        
        for item in arrayTask{
            let dict = item as! NSDictionary
            if("\(dict.value(forKey: "status")!)" == "Completed")
            {
                isDataExist = true
                break
            }
        }        
        
        if(isDataExist == true)
        {
            arrayTaskCopy.removeAllObjects()
            for item in arrayTask{
                let dict = item as! NSDictionary
                if("\(dict.value(forKey: "status")!)" == "Completed")
                {
                    arrayTaskCopy.add(dict)
                }
            }
            selectionType = 3
            self.constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tvTask.reloadData()
        }
        
       
//        if(arrayTaskCopy.count > 0)
//        {
//
//        }
//        else{
//            arrayTaskCopy = arrayTask.mutableCopy() as! NSMutableArray
//        }
    }
    
    // MARK: Segue method

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if(segue.identifier == "AddTaskVC")
        {
            if(strTitleForAddTask == "Add Task")
            {
               dictTaskForAddTask = NSDictionary()
            }
            let addTaskVC = segue.destination as! AddTaskVC
            addTaskVC.strTitle = strTitleForAddTask
            addTaskVC.dictTask = dictTaskForAddTask
        }
    }
    
    // MARK: get task

    func getAllTask()
    {
        if(isInternetAvailable())
        {
//             FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
            
            customDotLoaderShowOnFull(message: "", controller: self)
        
            db.getTasks { (aryTask) in
                
//                FTIndicator.dismissProgress()
                
                customeDotLoaderRemove()
                if(aryTask.count == 0)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No data available", viewcontrol: self)
                }
                else
                {
                    self.arrayTask = aryTask
                    self.arrayTaskCopy.removeAllObjects()
                    for item in self.arrayTask{
                        let dict = item as! NSDictionary
                        if("\(dict.value(forKey: "status")!)" == "Not Started")
                        {
                            self.arrayTaskCopy.add(dict)
                        }
                    }
                    if(self.arrayTaskCopy.count > 0)
                    {
                        self.constLeadingView.constant = 10
                        UIView.animate(withDuration: 0.3) {
                            self.view.layoutIfNeeded()
                        }
                       
                    }
                     self.tvTask.reloadData()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    func deleteTask(dict : NSMutableDictionary)
    {
        if(isInternetAvailable())
        {
//             FTIndicator.showProgress(withMessage: "Deleting Task...", userInteractionEnable: false)
            
            customDotLoaderShowOnFull(message: "", controller: self)
            
            db.deleteTask(dictTask: dict) { (status) in
                
//                FTIndicator.dismissProgress()
                customeDotLoaderRemove()

                if(status == true)
                {
                    let alertController = UIAlertController(title: "Message", message: "Task deleted successfully", preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                        
                      // self.getAllTask()
                        
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            db.getTasks { (aryTask) in
                
                
                if(aryTask.count == 0)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No data available", viewcontrol: self)
                }
                else
                {
                    self.arrayTask = aryTask
                    self.arrayTaskCopy.removeAllObjects()
                    if(self.selectionType == 1)
                    {
                        self.arrayTaskCopy.removeAllObjects()
                        for item in self.arrayTask{
                            let dict = item as! NSDictionary
                            if("\(dict.value(forKey: "status")!)" == "Not Started")
                            {
                                self.arrayTaskCopy.add(dict)
                            }
                        }
                    }
                    else if(self.selectionType == 2)
                    {
                        self.arrayTaskCopy.removeAllObjects()
                        for item in self.arrayTask{
                            let dict = item as! NSDictionary
                            if("\(dict.value(forKey: "status")!)" == "In Progress")
                            {
                                self.arrayTaskCopy.add(dict)
                            }
                        }
                    }
                    else{
                        self.arrayTaskCopy.removeAllObjects()
                        for item in self.arrayTask{
                            let dict = item as! NSDictionary
                            if("\(dict.value(forKey: "status")!)" == "Completed")
                            {
                                self.arrayTaskCopy.add(dict)
                            }
                        }
                    }
                    
                    self.tvTask.reloadData()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}
extension TaskVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTaskCopy.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let taskCell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath as IndexPath) as! TaskCell
        
        let dictTask = arrayTaskCopy.object(at: indexPath.row) as! NSDictionary
        taskCell.lblTaskTitle.text = ("\(dictTask.value(forKey: "title")!)")
        taskCell.lblTaskStatus.text = ("\(dictTask.value(forKey: "status")!)")
        
        taskCell.lblTaskDueDate.text = dateTimeConvertor(str: "\(dictTask.value(forKey: "duedate")!)", formet: "yyyy-MM-dd", strFormatToBeConverted: "MM/dd/yyyy")
        
        taskCell.lblTaskReminderDate.text = dateTimeConvertor(str: "\(dictTask.value(forKey: "reminderdate")!)", formet: "yyyy-MM-dd", strFormatToBeConverted: "MM/dd/yyyy")
        
        taskCell.lblTaskDescription.text = ("\(dictTask.value(forKey: "description")!)").count > 0 ? ("\(dictTask.value(forKey: "description")!)") : " "
        
        return taskCell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
     
        
           let dict = self.arrayTaskCopy.object(at: indexPath.row) as! NSDictionary
        
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
                
                self.dictTaskForAddTask = self.arrayTaskCopy.object(at: index.row) as! NSDictionary
                self.strTitleForAddTask = "Edit Task"
                self.performSegue(withIdentifier: "AddTaskVC", sender: nil)
            }
            edit.backgroundColor = appThemeColor
        
        
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            self.dictTaskForAddTask = self.arrayTaskCopy.object(at: index.row) as! NSDictionary
          
            let alertController = UIAlertController(title: "Message", message: "Are you sure you want to delete task?", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: { (alertAction) in
                
                self.deleteTask(dict: self.dictTaskForAddTask as! NSMutableDictionary)
                
            }))
            alertController.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (alertAction) in
                
                self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        delete.backgroundColor = UIColor.red
      
        if("\(dict.value(forKey: "status")!)" == "Completed")
        {
            return [delete]

        }
        else{
            return [edit, delete]
        }
        
    }
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
