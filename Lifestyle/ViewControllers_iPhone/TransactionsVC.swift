//
//  TransactionsVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 28/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import Alamofire

class TransactionCell: UITableViewCell {
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblAccName: UILabel!
    @IBOutlet weak var lblTransactionType: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!    
    @IBOutlet weak var lblTransactionNo: UILabel!
    
    @IBOutlet weak var lblWorkOrderNo: UILabel!
    
    @IBOutlet weak var consHghtDueDateTitle: NSLayoutConstraint!
}
class TransactionsVC: UIViewController {

    @IBOutlet weak var tblviewTransaction: UITableView!
    
    @IBOutlet weak var constLeadingView: NSLayoutConstraint!
    
    // variable
    var arrayTransactions = NSMutableArray()// this will contain all the data
    var arrayTransactionsCopy = NSMutableArray()// this will contain filtered data
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblviewTransaction.tableFooterView = UIView()
        if(isInternetAvailable()){
            getTransactions()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    // MARK: UIButton actions
    @IBAction func actionOnInvoice(_ sender: UIButton) {
        self.arrayTransactionsCopy.removeAllObjects()
        
        for item in self.arrayTransactions{
            
            if("\((item as! NSDictionary).value(forKey: "TransactionType")!)" == "Invoice")
            {
               arrayTransactionsCopy.add(item)
            }
        }
        if(arrayTransactionsCopy.count > 0)
        {
            constLeadingView.constant = 5
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tblviewTransaction.reloadData()
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnReceivePayment(_ sender: Any) {
        self.arrayTransactionsCopy.removeAllObjects()
        
        for item in self.arrayTransactions{
            
            if("\((item as! NSDictionary).value(forKey: "TransactionType")!)" == "Receive Payment")
            {
                self.arrayTransactionsCopy.add(item)
            }
        }
        
        if(arrayTransactionsCopy.count > 0)
        {
            constLeadingView.constant = (sender as! UIButton).frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tblviewTransaction.reloadData()
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnCreditMemo(_ sender: UIButton) {
        
        self.arrayTransactionsCopy.removeAllObjects()
        
        for item in self.arrayTransactions{
            
            if("\((item as! NSDictionary).value(forKey: "TransactionType")!)" == "Credit Memo")
            {
                self.arrayTransactionsCopy.add(item)
            }
        }
        if(arrayTransactionsCopy.count > 0)
        {
            constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            tblviewTransaction.reloadData()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is DocumentsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
        
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
//        self.navigationController?.pushViewController(docVC, animated: true)
    }
    
    @IBAction func actionOnPastService(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is WorkOrdersVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
        
        
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TaskVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
       
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    
    // MARK: - Web service to get dashboard data
    func getTransactions()
    {
        let arrayAccounts = getUserAccounts()
        if(arrayAccounts.count > 0)
        {
            print(arrayAccounts)
            
            if let url = NSURL(string:URL_GetTransactions){
                
                var request = URLRequest(url: url as URL)
                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                
                //  request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
                
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
                
//                FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
                
                customDotLoaderShowOnFull(message: "", controller: self)

                Alamofire.request(request)
                    .responseJSON { response in
                        
//                        FTIndicator.dismissProgress()
                        customeDotLoaderRemove()
                        switch response.result {
                        case .success(let responseObject):
                            
                            self.arrayTransactions = (responseObject as! NSArray).mutableCopy() as! NSMutableArray
                            self.arrayTransactionsCopy.removeAllObjects()
                            
                            for item in self.arrayTransactions{
                                
                                if("\((item as! NSDictionary).value(forKey: "TransactionType")!)" == "Invoice")
                                {
                                    self.arrayTransactionsCopy.add(item)
                                }
                            }
                            
                                DispatchQueue.main.async {
                                    self.tblviewTransaction.reloadData()
                                }
                            
                            
                            print(responseObject)
                            
                        case .failure(let error):
                            
                            print(error)
                        }
                }
            }
        }
    }
}
extension TransactionsVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTransactionsCopy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let transactionCell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath as IndexPath) as! TransactionCell
        
        var transactionCell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell") as? TransactionCell
        if(transactionCell == nil)
        {
            transactionCell = (UITableViewCell.init(style: .default, reuseIdentifier: "TransactionCell") as! TransactionCell)
        }
        
        let dictData = arrayTransactionsCopy.object(at: indexPath.row) as! NSDictionary
     
        transactionCell!.lblAccNo.text = ("\(dictData.value(forKey: "AccountNo")!)").count > 0 ? "\(dictData.value(forKey: "AccountNo")!)" : ""
      
        transactionCell!.lblAccName.text = ("\(dictData.value(forKey: "AccountName")!)").count > 0 ? "\(dictData.value(forKey: "AccountName")!)" : ""
        
        transactionCell!.lblTransactionType.text = ("\(dictData.value(forKey: "TransactionType")!)").count > 0 ? "\(dictData.value(forKey: "TransactionType")!)" : ""
        
        if(checkNullValue(str: "\(dictData.value(forKey: "Amount")!)")?.count == 0)
        {
            transactionCell!.lblAmount.text = "0.00"
        }
        else
        {
            transactionCell!.lblAmount.text = getCurrencyFormat(str: "\(dictData.value(forKey: "Amount")!)")
        }
        transactionCell!.lblStatus.text = ("\(dictData.value(forKey: "Status")!)").count > 0 ? "\(dictData.value(forKey: "Status")!)" : ""//
        
        if("\(dictData.value(forKey: "DueDateStr")!)" == "<null>" || ("\(dictData.value(forKey: "DueDateStr")!)").count == 0)
        {
           transactionCell!.lblDueDate.text = " "
        }
        else{
            transactionCell!.lblDueDate.text = "\(dictData.value(forKey: "DueDateStr")!)"
        }
        
        if("\(dictData.value(forKey: "TransactionDateStr")!)" == "<null>" || ("\(dictData.value(forKey: "TransactionDateStr")!)").count == 0)
        {
            transactionCell!.lblTransactionDate.text = " "
        }
        else{
             transactionCell!.lblTransactionDate.text = "\(dictData.value(forKey: "TransactionDateStr")!)"
        }
       
     
        
        if("\(dictData.value(forKey: "TransactionType")!)" == "Receive Payment")
        {
           transactionCell!.consHghtDueDateTitle.constant = 0.0
        }
        else
        {
           transactionCell!.consHghtDueDateTitle.constant = 21.0
        }
        
        if("\(dictData.value(forKey: "TransactionNo")!)" == "<null>" || ("\(dictData.value(forKey: "TransactionNo")!)").count == 0)
        {
            transactionCell!.lblTransactionNo.text = " "
        }
        else{
            transactionCell!.lblTransactionNo.text = "\(dictData.value(forKey: "TransactionNo")!)"
        }
        
        if("\(dictData.value(forKey: "WorkOrderNo")!)" == "<null>" || ("\(dictData.value(forKey: "WorkOrderNo")!)").count == 0)
        {
            transactionCell!.lblWorkOrderNo.text = " "
        }
        else{
            transactionCell!.lblWorkOrderNo.text = "\(dictData.value(forKey: "WorkOrderNo")!)"
        }
        
        return transactionCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    
}
