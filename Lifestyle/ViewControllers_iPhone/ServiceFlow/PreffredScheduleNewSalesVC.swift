//
//  PreffredScheduleNewSalesVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/13/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import CoreLocation


class PreffredScheduleNewSalesVC: UIViewController   {
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnAcceptContinue: UIButton!
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strLat = "0.0"
    var strLong = "0.0"
    
    
    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        tvList.tableFooterView = UIView()
        btnAcceptContinue.layer.cornerRadius =  btnAcceptContinue.frame.size.height/2
        
        
        getCurrentLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
    //MARK:
    //MARK: RelatedMap
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    func loadDataOnTabel(locations : [CLLocation])  {
        
        let letD = Double("\(strLat)")
        let longD = Double("\(strLong)")
        
        aryList = NSMutableArray()
        var dict = NSMutableDictionary()
        dict.setValue("12/21/2019\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Chris Slaughter", forKey: "TechName")
        dict.setValue("\(letD!)", forKey: "Lat")
        dict.setValue("\(longD!)", forKey: "Long")
        dict.setValue("Techimages", forKey: "TechImage")
        dict.setValue("Clark Pest", forKey: "CompanyName")
        
        aryList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("01/15/2020\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Michael Urban", forKey: "TechName")
        dict.setValue("\(letD!)", forKey: "Lat")
        dict.setValue("\(longD! + 0.1)", forKey: "Long")
        dict.setValue("Techimages1", forKey: "TechImage")
        dict.setValue("ABC Houston", forKey: "CompanyName")
        
        
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("01/21/2020\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Shelton Ware", forKey: "TechName")
        dict.setValue("\(letD! - 0.1)", forKey: "Lat")
        dict.setValue("\(longD!)", forKey: "Long")
        dict.setValue("Techimages2", forKey: "TechImage")
        dict.setValue("Rottler Pest", forKey: "CompanyName")
        
        
        aryList.add(dict)
        
        arySelectedList.add(aryList.object(at: 0)as! NSDictionary)
        
        
        
        self.tvList.reloadData()
    }
    
    func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
        let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
        alert.addAction(UIAlertAction (title: "Go to setting", style: .default, handler: { (nil) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }))
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
        }))
        viewcontrol.present(alert, animated: true, completion: nil)
    }
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                
                                
                            })
        })
    }
    
    
}
//MARK:
//MARK: UITableViewDelegate
extension PreffredScheduleNewSalesVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "SchedulePreffredCell", for: indexPath as IndexPath) as! SchedulePreffredCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        
        cell.lblDateTime.text = "\(dictData.value(forKey: "Date")!)"
        cell.lblTechName.text = "\(dictData.value(forKey: "TechName")!)"
        cell.imgTech.layer.cornerRadius = cell.imgTech.frame.width/2
        cell.imgTech.layer.borderColor  = UIColor.lightGray.cgColor
        cell.imgTech.layer.borderWidth = 1.0
        cell.imgTech.image = UIImage(named: "\(dictData.value(forKey: "TechImage")!)")
        
        
        
        if(arySelectedList.contains(dictData)){
            cell.imgCheck.image =  UIImage(named: "check.png")
        }else{
            cell.imgCheck.image =  UIImage(named: "uncheck.png")
        }
        cell.btnLink.tag = indexPath.row
        cell.btnLink.addTarget(self, action: #selector(didTapLinkButton(sender:)), for: .touchUpInside)
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        
        if(arySelectedList.contains(dictData)){
            arySelectedList.remove(dictData)
        }else{
            arySelectedList.add(dictData)
        }
        self.tvList.reloadData()
    }
    @objc func didTapLinkButton(sender : UIButton) {
        // self.openWeb(contentLink: "https://www.google.com")
        let dictData = aryList.object(at: sender.tag)as! NSDictionary
        
        
        let dict = NSMutableDictionary()
        dict.setValue("\(dictData.value(forKey: "TechName")!)", forKey: "name")
        dict.setValue(UIImage(named: "\(dictData.value(forKey: "TechImage")!)"), forKey: "TechImage")
        
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let SCVC = storyboard.instantiateViewController(withIdentifier: "EmployeeProfileVC") as! EmployeeProfileVC
        SCVC.dictData = dict
        SCVC.strComeFrom = "NewFlow"
        self.navigationController?.pushViewController(SCVC, animated: true)
        
        
        
    }
    
}



//MARK:
//MARK: UITableViewDelegate
extension PreffredScheduleNewSalesVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        if currentLocation != nil {
            self.strLat = "\(currentLocation!.coordinate.latitude)"
            self.strLong = "\(currentLocation!.coordinate.longitude)"
            // Zoom to user location
            self.loadDataOnTabel(locations : locations)
            locationManager.stopUpdatingLocation()
        }
    }
}
