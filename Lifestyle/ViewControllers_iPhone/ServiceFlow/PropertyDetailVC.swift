//
//  PropertyDetailVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class PropertyDetailVC: UIViewController {
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var btnConfirmContinue: UIButton!
    @IBOutlet weak var btnDropDownAddressSelect: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    
    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        btnConfirmContinue.layer.cornerRadius =  btnConfirmContinue.frame.size.height/2
        btnDropDownAddressSelect.layer.cornerRadius = 5.0
        btnDropDownAddressSelect.layer.borderWidth = 1.0
        btnDropDownAddressSelect.layer.borderColor = UIColor.gray.cgColor
        
    }
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    
    @IBAction func actionOnConfirm(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                
                                let dictServiceData = nsud.value(forKey: "LifeStyle_ServiceSelection")as! NSDictionary
                                
                                // Warranty FLow
                                
                                if( "\(dictServiceData.value(forKey: "id")!)" == "1"){
                                    let alert = UIAlertController(title: "This issue covered under your warranty. Do you want to report this? ", message: "", preferredStyle: UIAlertController.Style.alert)
                                    
                                    
                                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let SCVC = storyboard.instantiateViewController(withIdentifier: "SurveyComplainVC") as! SurveyComplainVC
                                        SCVC.dictWO  = NSDictionary()
                                        SCVC.strTitle = "Service Request"
                                        
                                        self.navigationController?.pushViewController(SCVC, animated: true)
                                        
                                    }))
                                    
                                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { action in
                                        for controller in self.navigationController!.viewControllers as Array {
                                            if controller.isKind(of: DashboardVC.self) {
                                                self.navigationController!.popToViewController(controller, animated: true)
                                                break
                                            }
                                        }
                                        
                                        
                                    }))
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                    
                                }
                                    
                                    
                                    // New Sales Flow
                                else if( "\(dictServiceData.value(forKey: "id")!)" == "2"){
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let SCVC = storyboard.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                    self.navigationController?.pushViewController(SCVC, animated: true)
                                    
                                }
                                    
                                    // Uberization Flow
                                else if( "\(dictServiceData.value(forKey: "id")!)" == "3"){
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let SCVC = storyboard.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                    self.navigationController?.pushViewController(SCVC, animated: true)
                                }
                                
                            })
        })
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func actionOnAddAddress(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                                
                                
                            })
        })
    }
    
    
    @IBAction func actionOnDropDownAddress(_ sender: UIButton) {
        
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.strTag = 21
        var dict = NSMutableDictionary()
        let aryList = NSMutableArray()
        
        dict.setValue("11802 Warfield St, San Antonio, TX 78216, United States", forKey: "address")
        
        aryList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("11815 Warfield St, San Antonio, TX 78216, United States", forKey: "address")
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("231 E Nakoma Dr, San Antonio, TX 78216, United States", forKey: "address")
        aryList.add(dict)
        
        
        
        
        vc.aryTBL = aryList
        self.present(vc, animated: false, completion: {})
        
    }
    
}


extension PropertyDetailVC: PopUpDelegate {
    func getDataFromPopUpDelegate(dict: NSDictionary, tag: Int) {
        lblAddress.text = "Address: \(dict.value(forKey: "address")!)"
    }
    
}

