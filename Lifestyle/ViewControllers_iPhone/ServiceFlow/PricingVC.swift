//
//  PricingVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class PricingVC: UIViewController {
    
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnAcceptContinue: UIButton!
    
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var showMoreTag = 99999
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        var dict = NSMutableDictionary()
        dict.setValue("Take care of problem only once", forKey: "title")
        dict.setValue("175.00", forKey: "initialPrice")
        dict.setValue("00.00", forKey: "MaintenancePrice")
        dict.setValue("Controlling Crazy/Raspberry and Leaf Cutter Ants Infestations can be difficult due to the size and habits of these specific ant colonies. For best results, we will perform 3 treatments within 60 days for initial control. Treatment will focus in turf areas, shrubs, and trees affected by Crazy Tawny/Leafcutter Ants. We will then do two more treatments 60 days apart from each other to best control this ant. This insect will be warranted for 90 days after the last treatment date.", forKey: "Detail")

        aryList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("Take care of problem & continue to protect my property on Quarterly basis", forKey: "title")
        dict.setValue("95.00", forKey: "initialPrice")
        dict.setValue("45.00", forKey: "MaintenancePrice")
        dict.setValue("Our Pest Control Service involves four steps- \n1. Inspection and evaluation- Assess the situation and identify conducive conditions to pests, Identify pests and any kind, evaluate sanitation and structural conditions that would contribute to pest infestations and Customize a pest control program.\n2. Treatment- Designed to eliminate invading pests using 7 treatment techniques.\n3. Maintaining control- of the property, yard perimeter of the structure attic and indoors.\n4. Constant Improvement- We are constantly evaluate and improve our program to ensure the very best service available. Covered Fire Ants in the yard, Roaches, Spiders, Silverfish, Scorpions, Weevils, Wasps, Pharaoh Ants, Carpenter Ants, Millipedes, Earwigs, Pill Bugs, Crickets.", forKey: "Detail")

        aryList.add(dict)
        tvList.tableFooterView = UIView()
        btnAcceptContinue.layer.cornerRadius =  btnAcceptContinue.frame.size.height/2
        arySelectedList.add(aryList.object(at: 0))
    }
    
   
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnAccept(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "AgreementVC") as! AgreementVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                                
                                
                            })
        })
    }
}
//MARK:
//MARK: UITableViewDelegate
extension PricingVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "PricingCell", for: indexPath as IndexPath) as! PricingCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        if(indexPath.row == 0){
            cell.lblTitle.text = "\(dictData.value(forKey: "title")!)"
            cell.lblInitial.text = "Cost: $\(dictData.value(forKey: "initialPrice")!)"
            cell.lblMaintenance.text = ""
        }else{
            cell.lblTitle.text = "\(dictData.value(forKey: "title")!)"
            cell.lblInitial.text = "Initial Price: $\(dictData.value(forKey: "initialPrice")!)"
            cell.lblMaintenance.text = "Maintenance Price: $\(dictData.value(forKey: "MaintenancePrice")!)"
        }
        
        // For Show More Detail
        cell.btnShowDetail.tag = indexPath.row
        cell.btnShowDetail.addTarget(self, action: #selector(didTapShowMoreButton(sender:)), for: .touchUpInside)
        if(indexPath.row == showMoreTag){
            cell.lblDetail.text = "\(dictData.value(forKey: "Detail")!)"
            cell.btnShowDetail.setTitle("", for: .normal)
        }else{
            cell.lblDetail.text = ""
            cell.btnShowDetail.setTitle("Show Detail", for: .normal)
        }
        
        
        
        if(arySelectedList.contains(aryList.object(at: indexPath.row))){
            cell.viewRadio.backgroundColor = appThemeColor
        }else{
            cell.viewRadio.backgroundColor = UIColor.groupTableViewBackground
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        arySelectedList = NSMutableArray()
        arySelectedList.add(aryList.object(at: indexPath.row))
        self.tvList.reloadData()
    }
    @objc func didTapShowMoreButton(sender : UIButton) {
        if(showMoreTag == sender.tag){
            showMoreTag = 99999
        }else{
            showMoreTag = sender.tag
        }
        
        
        self.tvList.reloadData()
    }
}


//MARK:
//MARK: UITableViewCell
class PricingCell: UITableViewCell {
    
    @IBOutlet weak var viewRadio: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInitial: UILabel!
    @IBOutlet weak var lblMaintenance: UILabel!
    @IBOutlet weak var btnShowDetail: UIButton!
    @IBOutlet weak var lblDetail: UILabel!
    
}
