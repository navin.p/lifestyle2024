//
//  TakeImageVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class TakeImageVC: UIViewController {
    
    
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!

    
    //MARK:
    //MARK : Variable
    var imagePicker: UIImagePickerController!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.layer.cornerRadius =  btnContinue.frame.size.height/2
        imageService.layer.cornerRadius = 6.0
        btnCamera.layer.cornerRadius =  5.0
        btnGallery.layer.cornerRadius =  5.0
    }
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                  })
        })
    }
    @IBAction func actionOnContinue(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity

                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                          let SCVC = storyboard.instantiateViewController(withIdentifier: "ServiceSelectionVC") as! ServiceSelectionVC
                                                          self.navigationController?.pushViewController(SCVC, animated: true)
                            })
        })
    }
    
    @IBAction func actionOnCamera(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                   selectImageFrom(.photoLibrary)
                   return
               }
               selectImageFrom(.camera)
       }
    @IBAction func actionOnGallery(_ sender: UIButton) {
                  selectImageFrom(.photoLibrary)
    }
    
    
    //MARK:
    //MARK: Extra Function
    func selectImageFrom(_ source: ImageSource){
           imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
           switch source {
           case .camera:
               imagePicker.sourceType = .camera
           case .photoLibrary:
               imagePicker.sourceType = .photoLibrary
           }
           present(imagePicker, animated: true, completion: nil)
       }
    
}

//MARK:
   //MARK: UIImagePickerControllerDelegate
 extension TakeImageVC: UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        imageService.image = selectedImage
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}
