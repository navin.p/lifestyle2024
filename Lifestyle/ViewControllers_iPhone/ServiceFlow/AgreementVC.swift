//
//  AgreementVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/12/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class AgreementVC: UIViewController  {
    
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var height_viewServiceTerms: NSLayoutConstraint!
    @IBOutlet weak var height_viewServiceDetail: NSLayoutConstraint!
    @IBOutlet weak var viewServiceDetail: UITextView!
    @IBOutlet weak var viewServiceTerms: UITextView!
    @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var imgSign: UIImageView!
    @IBOutlet weak var height_imageSign: NSLayoutConstraint!
  @IBOutlet weak var btnCheckBox: UIButton!


    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
      
        btnSubmit.layer.cornerRadius =  btnSubmit.frame.size.height/2
        height_imageSign.constant = 0.0
        btnCheckBox.setImage(UIImage(named: "uncheck.png"), for: .normal)
       
    }
    
    
    
    //MARK:
    //MARK : IBAction's
    
    @IBAction func actionOnBack(_ sender: UIButton) {
      UIButton.animate(withDuration: 0.2,
                          animations: {
                             sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
         },
                          completion: { finish in
                             UIButton.animate(withDuration: 0.2, animations: {
                                 sender.transform = CGAffineTransform.identity
                                 self.navigationController?.popViewController(animated: true)
                             })
         })
        
    }
    
    

    
    @IBAction func actionOnCheckTerms(_ sender: UIButton) {
        
   
        if(sender.currentImage == UIImage(named: "uncheck.png")){
                  btnCheckBox.setImage(UIImage(named: "check.png"), for: .normal)

              }else{
                  btnCheckBox.setImage(UIImage(named: "uncheck.png"), for: .normal)
              }
    }
    
  
    
    @IBAction func ActionTakeSign(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SCVC = storyboard.instantiateViewController(withIdentifier: "SignatureVC") as! SignatureVC
        SCVC.delegate = self
        self.present(SCVC, animated: true, completion: nil)
        
        
    }
    


    @IBAction func ActionSubmit(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                
                                
                                   let dictServiceData = nsud.value(forKey: "LifeStyle_ServiceSelection")as! NSDictionary
                                // NewSales Flow
                                if("\(dictServiceData.value(forKey: "id")!)" == "2"){
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                                 let SCVC = storyboard.instantiateViewController(withIdentifier: "PreffredScheduleNewSalesVC") as! PreffredScheduleNewSalesVC
                                                                 self.navigationController?.pushViewController(SCVC, animated: true)
                                }
                                 // Uberization Flow
                                
                                else if("\(dictServiceData.value(forKey: "id")!)" == "3"){
                                                                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                let SCVC = storyboard.instantiateViewController(withIdentifier: "PreffredScheduleVC") as! PreffredScheduleVC
                                                                                                self.navigationController?.pushViewController(SCVC, animated: true)
                                }
                                
                           
                                
                             
                                
                                
                            })
        })
    }
    
    @IBAction func ActionOnTerms_Conditions(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                       let SCVC = storyboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
         SCVC.strTitle = "Terms & Condition"
        SCVC.strContain = "TERMS OF AGREEMENT: The agreement shall be for an initial period of one year from the date of acceptance and shall renew itself from year to year unless written notice of cancellation is given by either party at least thirty (30) days prior to the anniversary date of this agreement. If Dream pest  fails to comply with the specifications, they shall be given a thirty (30) day notice, and if the unsatisfactory conditions have not been corrected, the customer reserves the right to cancel the agreement. It is the homeowner’s responsibility to make the foregoing premises available for service by Dream pest  at the intervals agreed to in this agreement. If the homeowner or a duly authorized agent fails to provide regular access, the effectiveness of the service will be severely reduced beyond the control of Dream pest . Termination of credit card charge must be in writing thirty (30) days prior to next service date.\nTIME IS OF THE ESSENCE OF THIS AGREEMENT: This agreement is limited to its written terms, and no verbal statement or arrangements shall be binding on either party hereto. Necessary price adjustments may be negotiated anytime after the first year. The initial price may be adjusted if the conditions have changed and or the bid is over one week since proposed.\nTERMS OF PAYMENT: In consideration of the above service(s), the undersigned agrees to pay in a timely manner. The initial/first payment is due at time of service. All services are plus tax. Monthly statements net fifteen (15) days.\nMATERIALS: The materials used shall conform to Federal, State and Local ordinances. All work performed shall be in a safe manner, according to manufacturer instructions, weather, and the most modern and effective procedures.\nDAMAGE CAUSED BY TERMITES, RODENTS OR INSECTS: This agreement does not provide customer any right to compensation for damage, replacement, or repairs to any time necessitated by Termites, Rodents or any other insects to the Customer’s property. Should walls need to be opened to remove dead rodents or inspect for termite damage Dream pest  is not responsible for those repairs. There will be an additional charge to open walls that will only be done with the Customer’s approval.\nLIMITATION FOR WASP COVERAGE: Wasp coverage under signature pest control is for visible exterior nests only. It does not include wasp/bee nests inside walls or soffit areas and also does not include removal of wasp/bee hives or nests. Wasp/bee nests in walls or soffits are bid on separate agreements.\nYOUR COOPERATION: Your cooperation is important to ensure the most effective results from Dream pest  service. In the event you have to reschedule the service, you must notify Dream pest  at least 72 hours prior to your scheduled appointment. You also warrant full cooperation with Dream pest  during the term of this Agreement, and agree to eliminate conditions from an structure that may contribute to Termite infestations, such as, standing water, faulty plumbing, leaks, dampness from condensation, direct wood to soil contact, landscaping above the foundation grade line, or as noted by the technician during inspection.\nACCESSIBILITY: Please make sure animals are secured and access to the property/gates are unlocked for service. If for any reason the animals are not secured and/or the gates are locked, prohibiting access/preventing service from being performed a full service amount will be charged to the customer. Dream pest  reserves the right to void your warranty if the regular service visits are not performed as scheduled.\nCLAIM PROCESSING: In the event that Dream pest  damages Customer’s property, the customer will need to contact Dream pest  within seventy-two (72) hours of the incident to make a claim. All claims must be in writing and either mailed, faxed or e-mailed to Dream pest. Dream pest  will send out a representative to determine the liability and/or if repairs are needed. Once approved by Dream pest , and if Dream pest  is liable, repairs will be made by an Dream pest  representative. If the customer and/or duly authorized agent chooses to repair damaged property without proper notification to Dream pest , then the customer assumes all liability and financial responsibility for the said repair.\nNO ORAL PROMISES OR AGREEMENTS: It is understood that this is the entire and only agreement of the parties regarding the subject matter herein, and this Agreement supersedes and replaces all other agreements, promises or representations. Customer did not and has not relied on any oral promises, representations or assurances that are set forth in writing in this Agreement.\nLIMITATION OF WARRANTIES, REMEDIES, LIABILITY AND DAMAGES: Seller warrants that all services and goods sold to Customer conform to the label description and the terms of this agreement. All other representations or warranties of any kind, express or implied, concerning this Agreement or the goods or services to be provided, including but not limited to, the implied warranty of merchantability or fitness of the goods or services for any particular purpose, hereby waived and excluded. Customer’s sole and exclusive remedy against Dream pest  for any complaint, claim or cause of action is to request that Dream pest  provide the goods and/or services again, or at Dream pest ’s discretion, it may instead return your money. Customer must notify Dream pest  in writing of any complaint, claim or cause of action within 30 days after the date Customer becomes aware of, or should have become aware of same, and failure to do so shall constitute an absolute and unconditional waiver and release of same. Dream pest ’s liability to Customer for any complaint, claim or cause of action shall be limited to two times the yearly payment due under this agreement, and Dream pest  shall have no liability to Customer for any claim or damages caused by Rodents or any other insects. For Termite Warranty information please refer to the attached Termite Warranty provision. Dream pest  shall not be liable to Customer for punitive, indirect, consequential, special, coincidental damages or expenses of any nature or kind, including but not limited to, loss of revenues, profits or income, crop or property damage, labor charges and/or freight charges. Warranty on Bed Bug Control: The Customer must maintain the coverage for six (6) weeks from the date of purchase before the Bed Bug warranty goes into effect. Any active infestation of Bed Bugs discovered prior to expiration of the initial six (6) week period is not covered.\nWarranty on Misting System: Dream pest  Warranties the misting system for the tubing, fittings and nozzles as long as the maintenance agreement is in force against defects of normal operating conditions The tank, pump, motor and timer have a One Year Limited Manufacturers Warranty. This agreement does not cover negligence on the Customer’s part such as cut lines, motor, pumps, or timer exposed to the elements etc...\nARBITRATION AND WAIVER OF JURY: The Customer and Dream pest  agree that any claim, dispute or controversy pertaining to or arising out of, directly, or indirectly, this Agreement, the good or services provided, any alleged representations or the relationship of the parties shall be submitted to binding arbitration to be conducted in local area in accordance with the Commercial Arbitration Rules before the American Arbitration Association with a single arbitrator. The prevailing party shall be entitled to recover all reasonable attorney’s fees, expenses and costs in connection with said proceeding, including but not limited to, the costs of arbitration, the arbitrator’s fees, and all costs and expenses incurred in the investigation, prosecution or defense of such claims. To the extent any Court fails to refer any lawsuit filed to arbitration, Customer agrees to waive the right to a trail by jury.\nTRANSFER PROVISION: In the event the Structure(s) is sold, this Agreement may be transferred to the buyer upon execution of a signed Transfer Agreement at no additional cost. All transfers must be in writing and either mailed, faxed or e-mailed to Dream pest. The transfer agreement must be executed prior to the expiration date of this agreement.\nMOLD/FUNGI DISCLAIMER: This property was not inspected for the presence or absence of health-related olds or fungi. We are not qualified to and do not render an opinion concerning indoor air quality or any health issues. Questions concerning the presence or absence of health-related issues, which may be associated with this property, the presence of mold, the release of mold spores or concerning indoor air quality should be directed to a properly trained Industrial Hygienist or the public health department.\nRodents Specific Terms And Conditions\nIt is important that you receive regular service at the times indicated above. Live traps must be checked, and the rodents relocated and released at least every 2 days. If Dream pest  is either not able to contact you or you cannot be available to do this on the follow-up dates above, please call us at 837-9500 to reset a time for this service.\nBoth the Rodent Exclusion and the Rodent Management Program may result in the death of the rodent(s) inside the home in an area that is not accessible to the service technician. This can result in an odor associated with the decomposition of the rodent. This condition is temporary but Dream pest is unable to guarantee that the condition will not occur. The Rodent Exclusion and the Rodent Management Programs do not cover, and Dream pest  will not be responsible for, damage to property caused by the rodents. Improperly fitted garage doors or broken plumbing lines must be corrected by the customer for the Exclusion Warranty to remain in effect. Roof repair, replacement, or home remodeling may result in additional exclusion work and an additional charge to keep the Exclusion Warranty in effect. This agreement shall be in effect for the specified period indicated above. In the case of the FULL RODENT EXCLUSION service may be renewed annually thirty (30) days prior to the anniversary date of this agreement, or subsequent renewals. In the event Dream pest  is unable to render service due to lack of availability of the premises, the full payment for a period of one year shall be deemed payable to Dream pest . A transfer or move from the Austin trade area nullifies the remaining term of this agreement unless the new owner contacts Dream pest  to transfer the service into their name within thirty (30) days of their taking possession of the property. With the rodent management program it is very important to check/replace the bait on a regular basis. Dream pest  reserves the right to void your warranty if the follow-up services are not performed as scheduled. This agreement is limited to its written terms and not by any other representation, oral, or otherwise In consideration of the foregoing service, the undersigned agrees to pay Dream pest in the following manner.\nTermite Specific Terms And Conditions\nTERMITE DAMAGE PROVISION: Subject to the conditions of this Agreement, Dream pest  will repair damage caused by a live infestation of subterranean termites that occurs after the treatment of the covered premises. Dream pest  is not responsible for (1) any past or existing damage to the structure(s) or its contents at the date of this Agreement caused by any wood destroying organisms, mold or insects, including termites whether hidden or visible; or (2) any costs or expenses incurred by Customer as a result of such damage or (3) any damage caused by or related to any or the conditions described in Paragraph 2 below. If at any time during the terms of this agreement, subterranean termites or Formosan termites appear and infest the structure(s). Dream pest agrees to repair any and all NEW termite damages related to the treated structure(s) (i.e., damage where live termites are found by representative of Dream pest  at the expense and direction of Dream pest ). The Customer must report any signs and /or evidence termites or damage when discovered immediately. All repairs are to be done with materials of a like kind and quality to those found in the damaged areas by Dream pest  approved contractors. THE TOTAL REPAIR OBLIGATION TO Dream pest  IS LIMITED TO A TOTAL OF $75,000.00 after a deductible payment of $250.00, which is the responsibility of the Customer. The $250.00 deductible applies to each separate damage occurrence of new damage. The $75,000.00 is an aggregate limitation for all occurrences of new damage during the term of this Agreement. Dream pest  will not repair any hidden damage discovered after the date of this Agreement unless alive infestation is found at the site of damage or unless live termites are found by Dream pest , all the damage shall be deemed to be old or existing damage not subject to any repairs under this Agreement. Dream pest  will not repair any existing damage identified on the diagram/graph attached hereto.\nCONDITIONS CONDUCIVE TO INFESTATION: The customer will fully cooperate with Dream pest  during the term of this Agreement, and will maintain the area(s) treated free from any factors contributing to infestation, such as wood, trash, lumber, direct wood-soil contact, cellulose, debris, or standing water under pier type structure. There must be a minimum clearance of four (4) inches between the ground and any untreated wood member, foam board, stucco on framed construction, and vinyl or similar material. Customer will notify Dream pest  of and eliminate faulty plumbing, leaks, and dampness from drains, condensation, or leaks from the roof or otherwise into, or under the area(s) treated. Customer agrees to maintain adequate ventilation and vapor barrier in crawl space of treated structures(s). Dream pest  may terminate this Agreement if Customer fails to correct any conditions including, but not limited to the conditions listed above, which contribute or may contribute to infestation, and Dream pest  will be released from any further obligation under the Agreement upon notice of termination to Customer. Notwithstanding Dream pest ’s decision to continue this Agreement, Customer agrees that Dream pest  will not be held responsible for any damage caused to the structure(s) treated as a result of any said conditions. Failure of Dream pest  to notify Customer of any of the above conditions does not relieve the Customer of responsibility under this paragraph, or waive Dream pest ’s right to terminate this Agreement. Upon written notice to the Customer, Dream pest may, at its discretion, terminate the provisions of this Agreement due to the presence of conditions conducive to infestation or damage to the property covered by the terms of this Agreement. Dream pest ’s failure to notify customer or cancel this Agreement does not constitute a waiver for these provisions.\nADDITIONS OR ALTERATIONS: This agreement covers only the structure(s) identified herein. Prior to the structure(s) being modified, altered, or otherwise changed, or removal or addition of soil within 6 inches of foundation, the Customer is to notify Dream pest  in writing. In event that such additions or alterations are made, Customer agrees to be responsible for additional treatment as a result of such additions or alterations, failure of which could result in termination of this Agreement. Damage resulting from additions or alterations will be the responsibility of the Customer. Such additions may result in additional treatment of the premise at the Customer’s expense. In the event of any such change, Dream pest  reserves right to adjust the annual renewal charge. The failure of Dream pest to notice any such additions or alterations does not release the Customer from the obligations set forth in this paragraph."
        
        
                                       self.navigationController?.pushViewController(SCVC, animated: true)
    }
     
   
}
//MARK:
  //MARK : Delegate
extension AgreementVC : SignatureVCDelegate{
    func getImageFromSignatureDelegate(image: UIImage) {
        self.imgSign.image = image
        self.height_imageSign.constant = 150.0
    }
    
    
}
