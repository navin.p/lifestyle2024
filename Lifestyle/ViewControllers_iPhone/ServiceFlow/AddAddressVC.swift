//
//  AddAddressVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/11/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {
    //MARK:
      //MARK : IBOutlet's
   
           @IBOutlet weak var btnSave: UIButton!
    //MARK:
    //MARK : LifeCycle's
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.layer.cornerRadius =  btnSave.frame.size.height/2
       
        // Do any additional setup after loading the view.
    }
    

   //MARK:
      //MARK : IBAction's
      @IBAction func actionOnBack(_ sender: UIButton) {
          
          UIButton.animate(withDuration: 0.2,
                           animations: {
                              sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
          },
                           completion: { finish in
                              UIButton.animate(withDuration: 0.2, animations: {
                                  sender.transform = CGAffineTransform.identity
                                  self.navigationController?.popViewController(animated: true)
                              })
          })
      }

}
