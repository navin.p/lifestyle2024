//
//  SignatureVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/11/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

protocol SignatureVCDelegate : class {
    func getImageFromSignatureDelegate(image : UIImage)
    
   

}




class SignatureVC: UIViewController ,YPSignatureDelegate{

    //MARK:
            //MARK : IBOutlet's
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!

    open weak var delegate:SignatureVCDelegate?

    
    //MARK:
    //MARK : LifeCycle's
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
        btnClear.layer.cornerRadius =  btnClear.frame.size.height/2
        btnSubmit.layer.cornerRadius =  btnSubmit.frame.size.height/2
        signatureView.layer.cornerRadius = 8.0
        signatureView.layer.borderColor = UIColor.darkGray.cgColor
        signatureView.layer.borderWidth = 1.0
    }
    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
                   
               }
    }
    // Function for clearing the content of signature view
    @IBAction func clearSignature(_ sender: UIButton) {
        // This is how the signature gets cleared
        self.signatureView.clear()
    }
    
    // Function for saving signature
    @IBAction func saveSignature(_ sender: UIButton) {
        // Getting the Signature Image from self.drawSignatureView using the method getSignature().
        if let signatureImage = self.signatureView.getSignature(scale: 10) {
            
            // Saving signatureImage from the line above to the Photo Roll.
            // The first time you do this, the app asks for access to your pictures.
            UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
          
            self.delegate?.getImageFromSignatureDelegate(image: signatureImage)

            self.dismiss(animated: true, completion: nil)
            
            
        }
    }
    // MARK: - Delegate Methods
      
    
      func didStart(_ view : YPDrawSignatureView) {
          print("Started Drawing")
      }
      
      // didFinish() is called rigth after the last touch of a gesture is registered in the view.
      // Can be used to enabe scrolling in a scroll view if it has previous been disabled.
      func didFinish(_ view : YPDrawSignatureView) {
          print("Finished Drawing")
      }
}
