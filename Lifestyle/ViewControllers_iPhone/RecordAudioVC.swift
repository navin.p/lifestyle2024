//
//  RecordAudioVC.swift
//  Lifestyle
//
//  Created by Rakesh Jain on 17/04/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import AVFoundation

class RecordAudioVC: UIViewController,AVAudioRecorderDelegate {
    
    @IBOutlet weak var lblTimer: UILabel!
    
    @IBOutlet weak var btnRecord: UIButton!
    
    @IBOutlet weak var btnStopAndSave: UIButton!
    
    @IBOutlet weak var imgViewRecord: UIImageView!
  
    var meterTimer = Timer()
    
    var audioRecorder: AVAudioRecorder!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnStopAndSave.isEnabled = false
        btnRecord.isEnabled = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imgViewRecord.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self!.imgViewRecord.transform = .identity
            },
                       completion: nil)
    }

    // MARK: UIButton action
   
    @IBAction func actionOnRecord(_ sender: UIButton) {       
        
        if(audioRecorder == nil)
        {
            audioRecordingSetUp()
        }
    }
    
    @objc func updateAudioMeter(timer: Timer)
    {
        if audioRecorder.isRecording
        {
            let min = Int(audioRecorder.currentTime / 60)
            if(min == 1)
            {
                finishRecordingAndSave()
            }
            else
            {
                let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
                let totalTimeString = String(format: "%02d:%02d", min, sec)
                lblTimer.text = totalTimeString
                audioRecorder.updateMeters()
            }            
        }
    }
    
    @IBAction func actionOnStopAndSave(_ sender: UIButton) {
       
        finishRecordingAndSave()
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func audioRecordingSetUp() {
        
          let audioFilename = getDocumentsDirectory()
          let audioSession = AVAudioSession.sharedInstance()
       
//        AVAudioSession.sharedInstance().requestRecordPermission { (granted) in
//
//            if(granted)
//            {
                do{
//                    try audioSession.setCategory(.playAndRecord, mode: .defaul)
                     try audioSession.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)
                    try audioSession.setActive(true)
                    

                    let settings = [
                        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                        AVSampleRateKey: 16000,
                        AVNumberOfChannelsKey: 1
                    ]
                   
                    self.audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
                    
                    self.audioRecorder.delegate = self
                    self.audioRecorder.prepareToRecord()
                    self.btnRecord.isEnabled = false
                    self.btnStopAndSave.isEnabled = true
                    self.audioRecorder.record()
                    self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
                    
                }
                catch let error as NSError {
                    
                    print(error.localizedDescription)
                }
          //  }
       // }
    }
   
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        print("recording has been finished")
    }
    
    func finishRecordingAndSave()
    {
        audioRecorder.stop()
        meterTimer.invalidate()
       
        let alertController = UIAlertController(title: "", message: "Audio saved successfully.", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alertAction) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)

    }
}


