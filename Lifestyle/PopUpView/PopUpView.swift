//
//  PopUpView.swift
//  GNGPL
//
//  Created by Saavan Patidar on 12/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

// tag 4 for source
import UIKit
protocol PopUpDelegate : class{
    func getDataFromPopUpDelegate(dict : NSDictionary , tag : Int)
}


class PopUpView: UIViewController {
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var height_ForTBLVIEW: NSLayoutConstraint!
    @IBOutlet weak var viewFortv: CardView!
    
    
    var strTitle = String()
    var aryTBL = NSMutableArray()
    var strTag = Int()
    var arySelectedSource = NSMutableArray()
    
    
    weak var handleSelectionOnEditOpportunity: refreshEditopportunity?
    weak var delegate: PopUpDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        btnTitle.setTitle(strTitle, for: .normal)
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        height_ForTBLVIEW.constant  = 0.0
        height_ForTBLVIEW.constant = CGFloat((aryTBL.count * 50 ) + 35 )
        if  height_ForTBLVIEW.constant > self.view.frame.size.height - 150  {
            height_ForTBLVIEW.constant = self.view.frame.size.height - 150
        }
        self.viewFortv.frame = CGRect(x: 15, y: Int(self.view.frame.height) / 2 - Int(self.height_ForTBLVIEW.constant) / 2 , width: Int(self.view.frame.width) - 30 , height: Int(self.height_ForTBLVIEW.constant))
        self.viewFortv.center = self.view.center
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.dismiss(animated: false) { }        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        if(strTag == 21){ // For Select Address
            let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "address")!)"

        }else{
            cell.popupCell_lbl_Title.text = "\(aryTBL.object(at: indexPath.row))"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
            
            
            if(self.strTag == 21){ // For Select Address
                let dict = self.aryTBL.object(at: indexPath.row)as! NSDictionary
                self.delegate?.getDataFromPopUpDelegate(dict: dict, tag: self.strTag)

                  }else{
                     self.handleSelectionOnEditOpportunity?.refreshViewOnEditOpportunitySelection(str : "\(self.aryTBL.object(at: indexPath.row))")
                  }
                  
          
            
            self.dismiss(animated: false) {}
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
class popupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}


