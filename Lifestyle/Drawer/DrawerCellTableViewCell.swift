//
//  DrawerCellTableViewCell.swift
//  WireFraming
//
//  Created by admin on 26/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class DrawerCellTableViewCell: UITableViewCell {
  
    @IBOutlet weak var imgProile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblImageName: UILabel!

    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnimg: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none

        // Configure the view for the selected state
    }

}
