//
//  FirebaseDB.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 06/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class FirebaseDB
{
    private var dbRef : DatabaseReference!
    private var taskRef : DatabaseReference!
    private var userKey = ""
    
    init() {
        
        self.dbRef = Database.database().reference()
        userKey = (Auth.auth().currentUser?.uid)!
        taskRef = self.dbRef.child("task")
    }
    
    func getUserAccounts(completinHandler: @escaping (Bool) ->Void)
    {
            let arrayusers = NSMutableArray()
            let dictaccount = NSMutableDictionary()
         dbRef.child("users").child(userKey).child("accounts").observeSingleEvent(of: .value) { (dataSnapshot) in
            
             for item in dataSnapshot.children.allObjects as! [DataSnapshot]
             {
                print(item)
                print(item.key)
                print(item.value!)
                
                let dictuser = item.value as! NSDictionary
                
                dictaccount.setValue("\(dictuser.value(forKey: "accountnumber")!)", forKey: "AccountNo")
                dictaccount.setValue("\(dictuser.value(forKey: "companykey")!)", forKey: "CompanyKey")
                dictaccount.setValue("\(dictuser.value(forKey: "softkey")!)", forKey: "AppKey")
                
                arrayusers.add(dictaccount.mutableCopy())
               
            }
            
            nsud.removeObject(forKey: "useraccounts")
            nsud.synchronize()
           
            nsud.set(arrayusers, forKey: "useraccounts")
            nsud.synchronize()
            completinHandler(true)
        }
    }
    
    func addTask(dictTask: NSMutableDictionary, completinHandler: @escaping (Bool) -> Void)
    {
        dictTask.setValue(taskRef.childByAutoId().key, forKey: "taskid")
      
        taskRef.child(userKey).childByAutoId().setValue(dictTask) { (error, ref) in
           
            if(error == nil)
            {
                completinHandler(true)
            }
            else
            {
                completinHandler(false)
            }
        }
        
//        taskRef.child(userKey).childByAutoId().updateChildValues(dictTask as! [AnyHashable : Any]) { (error, ref) in
//
//            if(error == nil)
//            {
//                completinHandler(true)
//            }
//            else
//            {
//                completinHandler(false)
//            }
//        }
    }
    
    func updateTask(dictTask: NSMutableDictionary, completinHandler: @escaping (Bool) -> Void)
    {
          let uniqueKey = "\(dictTask.value(forKey: "uniquekey")!)"
          print(uniqueKey)
          print(dictTask)

          dictTask.removeObject(forKey: "uniquekey")
          print(uniqueKey)
          print(dictTask)

         taskRef.child(userKey).child(uniqueKey).updateChildValues(dictTask as! [AnyHashable : Any]) { (error, dataRef) in
          
            if(error == nil)
            {
                completinHandler(true)
            }
            else
            {
                completinHandler(false)
            }
        }
    }
    
    func deleteTask(dictTask: NSMutableDictionary, completinHandler: @escaping (Bool) -> Void)
    {
        let uniqueKey = "\(dictTask.value(forKey: "uniquekey")!)"
        taskRef.child(userKey).child(uniqueKey).removeValue { (error, dataRef) in
            
            if(error == nil)
            {
                completinHandler(true)
            }
            else
            {
                completinHandler(false)
            }
        }
    }
    
    func getTasks(completionHandler: @escaping (NSMutableArray) -> Void)
    {
        let arrayTask = NSMutableArray()
        let dictTask = NSMutableDictionary()
        
        self.taskRef.child(userKey).observeSingleEvent(of: .value) { (snapshot) in
            
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                print(item.key)
                print(item.value!)
                let dictTaskTemp = item.value as! NSDictionary
                let uniqueKey = item.key
                
                let description = dictTaskTemp.value(forKey: "description") as! String
                let duedate = dictTaskTemp.value(forKey: "duedate") as! String
                let reminderdate = dictTaskTemp.value(forKey: "reminderdate") as! String
                let status = dictTaskTemp.value(forKey: "status") as! String
                let taskid = dictTaskTemp.value(forKey: "taskid") as! String
                let title = dictTaskTemp.value(forKey: "title") as! String
                
                dictTask.setValue(uniqueKey, forKey: "uniquekey")
                dictTask.setValue(description, forKey: "description")
                dictTask.setValue(duedate, forKey: "duedate")
                dictTask.setValue(reminderdate, forKey: "reminderdate")
                dictTask.setValue(status, forKey: "status")
                dictTask.setValue(taskid, forKey: "taskid")
                dictTask.setValue(title, forKey: "title")
                arrayTask.add(dictTask.mutableCopy())
                print(description)
            }
            
            
//            if let data = snapshot.value as? [String: Any] {
//                let dataArray = Array(data)
//                print(dataArray)
//
//                for item in dataArray
//                {
//                    print(item)
//                    let dictTaskTemp = item.value as! NSDictionary
//                    let uniqueKey = item.key
//
//                    let description = dictTaskTemp.value(forKey: "description") as! String
//                    let duedate = dictTaskTemp.value(forKey: "duedate") as! String
//                    let reminderdate = dictTaskTemp.value(forKey: "reminderdate") as! String
//                    let status = dictTaskTemp.value(forKey: "status") as! String
//                   // let taskid = dictTaskTemp.value(forKey: "taskid") as! String
//                    let title = dictTaskTemp.value(forKey: "title") as! String
//
//                    dictTask.setValue(uniqueKey, forKey: "uniquekey")
//                    dictTask.setValue(description, forKey: "description")
//                    dictTask.setValue(duedate, forKey: "duedate")
//                    dictTask.setValue(reminderdate, forKey: "reminderdate")
//                    dictTask.setValue(status, forKey: "status")
//                   // dictTask.setValue(taskid, forKey: "taskid")
//                    dictTask.setValue(title, forKey: "title")
//                    arrayTask.add(dictTask.mutableCopy())
//                    print(description)
//
//                }
//            }
            completionHandler(arrayTask)
        }
    }
    
}
